﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>wwDataBinder Samples</title>
    <link href="Westwind.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>        
    <h1 class="gridheader" style="background:Darkblue url(images/floralbackground.gif)">wwDataBinder Samples</h1>
    <div style="margin-left: 20px;width:500px;">
        <br />
        <br />
        <h3 >Databinding Samples</h3>
           <ul>
            <li>
                <b><a href="NorthwindInventoryData.aspx">Northwind Data Binding Sample</a></b><br />
                This sample demonstrates control databinding to data row fields of a datatable.
                It also demonstrates automatic binding error management and display of error messages, 
                as well as manually adding binding errors to display. Also shows binding to a Page
                property and variouse validation rules applied through server code.<br /><br />
            </li>
             <li>
                <b><a href="NorthwindInventoryEntity.aspx">Northwind Entity Object Binding Sample</a></b><br />
                This the same exact example as above except it binds to an Entity object of a business
                object. In fact this sample is nearly identical in code and is meant mainly to demonstrate
                that you can bind any kind of object easily as long as you can reference it from the Page.
                <br />
                <br />
            </li>
            <li> <img src="images/help.gif" /> <a href="docs/index.htm"><b>wwDataBinder Documentation</b></a><br />
            Link to the class documenation for the wwDataBinder class and its support classes.
            
            </li>
           </ul>         
      </div>
      <hr />
      <div style="float:right;padding-right:10px;">
      <a href="_readme.htm" style="text-decoration:none;"><img src="images/help.gif" border="0" /> Sample Configuration</a>
      </div>

    </div>
    </form>
</body>
</html>
