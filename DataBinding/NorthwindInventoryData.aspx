<%@ Page Language="C#" AutoEventWireup="true" 
         CodeFile="NorthwindInventoryData.aspx.cs" 
         Inherits="NorthwindInventoryData" 
         EnableViewState="true" %>
<%@ Register Assembly="wwDataBinder" Namespace="MsdnMag.Web.Controls" TagPrefix="ww" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Northwind Inventory (Data)</title>
    <link href="Westwind.css" rel="stylesheet" type="text/css" />
    <style>
        .columnlabel { background: darkblue }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h1>Northwind Product List Data Binding</h1>
    <small><a href="default.aspx">Home</a> | <a href="NorthwindInventoryData.aspx">Reload Form</a></small>
        <br />
    <br />
    <div style="padding-left:50px; width:550px" >

    <ww:wwErrorDisplay ID="ErrorDisplay" runat='server' Width="500px"
                       UserMessage="Please correct the following:" 
                       Center="true"></ww:wwErrorDisplay>
    <br />
    <table class="blackborder" style="width:550px;text-align:left;" cellpadding="5">
    <tr>
        <td style="width:125px" class="gridheader"></td>
        <td class="gridheader"><asp:DropDownList ID="txtProductId" runat="server" Width="350px" AutoPostBack="True" OnSelectedIndexChanged="txtProductId_SelectedIndexChanged">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td align="right" class="blockheader" style="width:125px">Description:</td>
        <td><asp:TextBox runat="server" ID="txtProductName" Width="350px"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="right" class="blockheader" style="width:125px">Category:</td>
        <td><asp:DropDownList runat="server" ID="txtCategoryId"  Width="350px"></asp:DropDownList></td>
    </tr>
    <tr>
        <td align="right" class="blockheader" style="width:125px">Supplier:</td>
        <td><asp:DropDownList runat="server" ID="txtSupplierId"  Width="350px"></asp:DropDownList></td>
    </tr>
    <tr>
        <td align="right" class="blockheader">Price:</td>
        <td><asp:TextBox runat="server" ID="txtUnitPrice" Width="75px"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp; &nbsp;
            Units on hand: <asp:TextBox runat="server" ID="txtUnitsInStock" Width="75px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td align="right" class="blockheader">Reorder Level:</td>
        <td><asp:TextBox runat="server" ID="txtReorderLevel" Width="75px"></asp:TextBox>&nbsp;&nbsp;&nbsp;
            Reorder Expected: <asp:TextBox runat="server" ID="txtReorderExpected" Width="75px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td align="right" class="blockheader" style="width:125px">Discontinued:</td>
        <td style="border-bottom: dashed 1px;"><asp:CheckBox runat="server" ID="chkDiscontinued" Width="350px" Text="Discontinued"  /></td>
    </tr>
    <tr >
        <td align="right" class="blockheader" style="width:125px">
            Time:</td>
        <td><asp:TextBox runat="server" ID="txtRightNow" Width="160px" /> <small>(Page Property binding)</small></td>
    </tr>
    
    </table>
        <br />
        <asp:Button ID="btnSubmit" runat="server" Text="Save" OnClick="btnSubmit_Click" Height="36px" Width="119px" />
        <br />
    </div>
    
    </div>
 <ww:wwDataBinder ID="DataBinder"
    runat="server" OnValidateControl="DataBinder_ValidateControl">
    <DataBindingItems>
       <ww:wwDataBindingItem runat="server"
          BindingSource="Item.DataRow"
          BindingSourceMember="ProductName"
          ControlId="txtProductName" IsRequired="True"
          ErrorMessageLocation="RedTextAndIconBelow">
       </ww:wwDataBindingItem>
       <ww:wwDataBindingItem runat="server"
          BindingProperty="SelectedValue"
          BindingSource="Item.DataRow"
          UserFieldName="Category" BindingSourceMember="CategoryId"
          ControlId="txtCategoryId">
       </ww:wwDataBindingItem>
       <ww:wwDataBindingItem runat="server"
          BindingSource="Item.DataRow"
          BindingSourceMember="UnitPrice"
          ControlId="txtUnitPrice" DisplayFormat="{0:f2}"
          IsRequired="True" UserFieldName="Unit Price">
       </ww:wwDataBindingItem>
       <ww:wwDataBindingItem runat="server"
          BindingProperty="Checked" 
          BindingSource='Item.DataSet.Tables[&quot;Products&quot;].Rows[0]'
          BindingSourceMember="Discontinued"
          ControlId="chkDiscontinued"
          UserFieldName="Discontinued">
       </ww:wwDataBindingItem>
       <ww:wwDataBindingItem runat="server"
          BindingSource="this" BindingSourceMember="RightNow"
          ControlId="txtRightNow" UserFieldName="Right Now">
       </ww:wwDataBindingItem>
       <ww:wwDataBindingItem runat="server"
          BindingSource="Item.Entity"
          BindingSourceMember="UnitsInStock"
          ControlId="txtUnitsInStock"
          DisplayFormat="{0:f0}">
       </ww:wwDataBindingItem>
       <ww:wwDataBindingItem runat="server"
          ControlId="txtSupplierId" BindingSource="Item.DataRow"
          BindingSourceMember="SupplierId"
          BindingProperty="SelectedValue">
       </ww:wwDataBindingItem>
       <ww:wwDataBindingItem runat="server"
          BindingSource="Item.DataRow"
          BindingSourceMember="ReorderLevel"
          ControlId="txtReorderLevel">
       </ww:wwDataBindingItem>
       <ww:wwDataBindingItem runat="server"
          BindingSource="Item.DataRow"
          BindingSourceMember="ReorderExpected"
          ControlId="txtReorderExpected"
          DisplayFormat="{0:d}" UserFieldName="Reorder Expected">
       </ww:wwDataBindingItem>
       <ww:wwDataBindingItem runat="server" ControlId="ErrorDisplay">
       </ww:wwDataBindingItem>
    </DataBindingItems>
 </ww:wwDataBinder>
        
        <hr />
        <div style="margin:20px 40px;">
        This page demonstrates the wwDataBinder control for binding. In this sample most controls bind to 
        a Item.DataRow object of a business object. The object loads up the ADO.NET DataRow and the form
        then binds to the various fields of the DataRow. The binder also binds to a Page property (the Current Time)
        on the bottom to demonstrate binding to a non-data item. A separate form demonstrates binding to an 
        <a href="NorthWindInventoryEntity.aspx">Entity object</a>.
        <br />
        <br />
        The wwDataBinder also manages binding errors and various validation errors. To try this out try changing
        a few values on the form to invalid values. A few things to try to create 'binding errors': 
        <ul>
        <li>Remove the description (IsRequired Binding Error)</li>
        <li>Try to save the first item (Manual Validation Error)</li>
        <li>Enter an invalid number or date value (Binding Error)</li>
        <li>Change Category to Dairy Products (ValidateControl event)</li>
        <li>Change the Reorder Expected date over one year out (Business Rule Validation Error)</li>
        </ul>
        <br />
        The binder can also manage existing Validator controls which are automaticly pulled into the 
        BindingErrors if not valid when the Unbind method is called.
        </div>
        <hr />
    </form>
</body>
</html>
