using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Westwind.BusinessObjects;
using DataEntities;

/// <summary>
/// Supplier Business Object
/// </summary>
public class busSupplier : wwBusiness<SuppliersEntity>
{
    public busSupplier()
    {
        this.ConnectionString = ConfigurationManager.ConnectionStrings["AjaxAspNet"].ConnectionString;
        this.ConnectType = ServerTypes.SqlServer;
        this.NewRowBlankValues = true;

        this.Tablename = "Suppliers";
        this.PkField = "SupplierID";
        this.PkType = PkFieldTypes.intType;
    }

    /// <summary>
    /// Return a list of suppliers with CompanyName and SupplierId into a specified table
    /// </summary>
    /// <returns></returns>
    public int GetSupplierList(string Table)
    {
        return this.Execute("select SupplierId,CompanyName from " + this.Tablename + " order by CompanyName", Table);
    }

}
