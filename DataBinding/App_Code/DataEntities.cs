//
// *** Auto Generated Code by DataRowContainerGenerator
// *** Created by West Wind Technologies, http://www.west-wind.com
// *** 
// *** Generated on: 7/31/2006 7:35:13 PM
//

using System;
using System.Data;
using System.Collections;
using System.Reflection;
using System.Xml.Serialization;
using Westwind.BusinessObjects;

namespace DataEntities
{

	[Serializable]
	public class CategoriesEntity : wwDataRowContainer 
	{			
		public CategoriesEntity() : base() {}
		public CategoriesEntity(DataRow Row) : base(Row) {}

		public Int32 CategoryID
		{
			get
			{
				if (this.DataRow == null)
					return this._CategoryID;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.CategoryIDColumn) )
						return 0;
					return (Int32) this.DataRow[this.CategoryIDColumn];
				}
				else
				{
					if (this.DataRow.IsNull("categoryid") )
						return 0;
				
					return (Int32) this.DataRow["categoryid"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["categoryid"] = value;
				
				this._CategoryID = value;					
			}
		}
		private Int32 _CategoryID;

		public String CategoryName
		{
			get
			{
				if (this.DataRow == null)
					return this._CategoryName;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.CategoryNameColumn) )
						return null;
					return (String) this.DataRow[this.CategoryNameColumn];
				}
				else
				{
					if (this.DataRow.IsNull("categoryname") )
						return null;
				
					return (String) this.DataRow["categoryname"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["categoryname"] = value;
				
				this._CategoryName = value;					
			}
		}
		private String _CategoryName;

		public String Description
		{
			get
			{
				if (this.DataRow == null)
					return this._Description;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.DescriptionColumn) )
						return null;
					return (String) this.DataRow[this.DescriptionColumn];
				}
				else
				{
					if (this.DataRow.IsNull("description") )
						return null;
				
					return (String) this.DataRow["description"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["description"] = value;
				
				this._Description = value;					
			}
		}
		private String _Description;

		public Byte[] Picture
		{
			get
			{
				if (this.DataRow == null)
					return this._Picture;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.PictureColumn) )
						return null;
					return (Byte[]) this.DataRow[this.PictureColumn];
				}
				else
				{
					if (this.DataRow.IsNull("picture") )
						return null;
				
					return (Byte[]) this.DataRow["picture"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["picture"] = value;
				
				this._Picture = value;					
			}
		}
		private Byte[] _Picture;


		// *** Column Definitions
		DataColumn CategoryIDColumn;
		DataColumn CategoryNameColumn;
		DataColumn DescriptionColumn;
		DataColumn PictureColumn;


		protected override void CreateColumns() 
		{
			CategoryIDColumn = this.DataRow.Table.Columns["CategoryID"];
			CategoryNameColumn = this.DataRow.Table.Columns["CategoryName"];
			DescriptionColumn = this.DataRow.Table.Columns["Description"];
			PictureColumn = this.DataRow.Table.Columns["Picture"];
			this.ColumnsCreated = true;
		}

	}

	[Serializable]
	public class CustomersEntity : wwDataRowContainer 
	{			
		public CustomersEntity() : base() {}
		public CustomersEntity(DataRow Row) : base(Row) {}

		public String CustomerID
		{
			get
			{
				if (this.DataRow == null)
					return this._CustomerID;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.CustomerIDColumn) )
						return null;
					return (String) this.DataRow[this.CustomerIDColumn];
				}
				else
				{
					if (this.DataRow.IsNull("customerid") )
						return null;
				
					return (String) this.DataRow["customerid"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["customerid"] = value;
				
				this._CustomerID = value;					
			}
		}
		private String _CustomerID;

		public String CompanyName
		{
			get
			{
				if (this.DataRow == null)
					return this._CompanyName;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.CompanyNameColumn) )
						return null;
					return (String) this.DataRow[this.CompanyNameColumn];
				}
				else
				{
					if (this.DataRow.IsNull("companyname") )
						return null;
				
					return (String) this.DataRow["companyname"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["companyname"] = value;
				
				this._CompanyName = value;					
			}
		}
		private String _CompanyName;

		public String ContactName
		{
			get
			{
				if (this.DataRow == null)
					return this._ContactName;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.ContactNameColumn) )
						return null;
					return (String) this.DataRow[this.ContactNameColumn];
				}
				else
				{
					if (this.DataRow.IsNull("contactname") )
						return null;
				
					return (String) this.DataRow["contactname"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["contactname"] = value;
				
				this._ContactName = value;					
			}
		}
		private String _ContactName;

		public String ContactTitle
		{
			get
			{
				if (this.DataRow == null)
					return this._ContactTitle;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.ContactTitleColumn) )
						return null;
					return (String) this.DataRow[this.ContactTitleColumn];
				}
				else
				{
					if (this.DataRow.IsNull("contacttitle") )
						return null;
				
					return (String) this.DataRow["contacttitle"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["contacttitle"] = value;
				
				this._ContactTitle = value;					
			}
		}
		private String _ContactTitle;

		public String Address
		{
			get
			{
				if (this.DataRow == null)
					return this._Address;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.AddressColumn) )
						return null;
					return (String) this.DataRow[this.AddressColumn];
				}
				else
				{
					if (this.DataRow.IsNull("address") )
						return null;
				
					return (String) this.DataRow["address"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["address"] = value;
				
				this._Address = value;					
			}
		}
		private String _Address;

		public String City
		{
			get
			{
				if (this.DataRow == null)
					return this._City;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.CityColumn) )
						return null;
					return (String) this.DataRow[this.CityColumn];
				}
				else
				{
					if (this.DataRow.IsNull("city") )
						return null;
				
					return (String) this.DataRow["city"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["city"] = value;
				
				this._City = value;					
			}
		}
		private String _City;

		public String Region
		{
			get
			{
				if (this.DataRow == null)
					return this._Region;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.RegionColumn) )
						return null;
					return (String) this.DataRow[this.RegionColumn];
				}
				else
				{
					if (this.DataRow.IsNull("region") )
						return null;
				
					return (String) this.DataRow["region"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["region"] = value;
				
				this._Region = value;					
			}
		}
		private String _Region;

		public String PostalCode
		{
			get
			{
				if (this.DataRow == null)
					return this._PostalCode;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.PostalCodeColumn) )
						return null;
					return (String) this.DataRow[this.PostalCodeColumn];
				}
				else
				{
					if (this.DataRow.IsNull("postalcode") )
						return null;
				
					return (String) this.DataRow["postalcode"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["postalcode"] = value;
				
				this._PostalCode = value;					
			}
		}
		private String _PostalCode;

		public String Country
		{
			get
			{
				if (this.DataRow == null)
					return this._Country;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.CountryColumn) )
						return null;
					return (String) this.DataRow[this.CountryColumn];
				}
				else
				{
					if (this.DataRow.IsNull("country") )
						return null;
				
					return (String) this.DataRow["country"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["country"] = value;
				
				this._Country = value;					
			}
		}
		private String _Country;

		public String Phone
		{
			get
			{
				if (this.DataRow == null)
					return this._Phone;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.PhoneColumn) )
						return null;
					return (String) this.DataRow[this.PhoneColumn];
				}
				else
				{
					if (this.DataRow.IsNull("phone") )
						return null;
				
					return (String) this.DataRow["phone"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["phone"] = value;
				
				this._Phone = value;					
			}
		}
		private String _Phone;

		public String Fax
		{
			get
			{
				if (this.DataRow == null)
					return this._Fax;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.FaxColumn) )
						return null;
					return (String) this.DataRow[this.FaxColumn];
				}
				else
				{
					if (this.DataRow.IsNull("fax") )
						return null;
				
					return (String) this.DataRow["fax"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["fax"] = value;
				
				this._Fax = value;					
			}
		}
		private String _Fax;


		// *** Column Definitions
		DataColumn CustomerIDColumn;
		DataColumn CompanyNameColumn;
		DataColumn ContactNameColumn;
		DataColumn ContactTitleColumn;
		DataColumn AddressColumn;
		DataColumn CityColumn;
		DataColumn RegionColumn;
		DataColumn PostalCodeColumn;
		DataColumn CountryColumn;
		DataColumn PhoneColumn;
		DataColumn FaxColumn;


		protected override void CreateColumns() 
		{
			CustomerIDColumn = this.DataRow.Table.Columns["CustomerID"];
			CompanyNameColumn = this.DataRow.Table.Columns["CompanyName"];
			ContactNameColumn = this.DataRow.Table.Columns["ContactName"];
			ContactTitleColumn = this.DataRow.Table.Columns["ContactTitle"];
			AddressColumn = this.DataRow.Table.Columns["Address"];
			CityColumn = this.DataRow.Table.Columns["City"];
			RegionColumn = this.DataRow.Table.Columns["Region"];
			PostalCodeColumn = this.DataRow.Table.Columns["PostalCode"];
			CountryColumn = this.DataRow.Table.Columns["Country"];
			PhoneColumn = this.DataRow.Table.Columns["Phone"];
			FaxColumn = this.DataRow.Table.Columns["Fax"];
			this.ColumnsCreated = true;
		}

	}

	[Serializable]
	public class ProductsEntity : wwDataRowContainer 
	{			
		public ProductsEntity() : base() {}
		public ProductsEntity(DataRow Row) : base(Row) {}

		public Int32 ProductID
		{
			get
			{
				if (this.DataRow == null)
					return this._ProductID;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.ProductIDColumn) )
						return 0;
					return (Int32) this.DataRow[this.ProductIDColumn];
				}
				else
				{
					if (this.DataRow.IsNull("productid") )
						return 0;
				
					return (Int32) this.DataRow["productid"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["productid"] = value;
				
				this._ProductID = value;					
			}
		}
		private Int32 _ProductID;

		public String ProductName
		{
			get
			{
				if (this.DataRow == null)
					return this._ProductName;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.ProductNameColumn) )
						return null;
					return (String) this.DataRow[this.ProductNameColumn];
				}
				else
				{
					if (this.DataRow.IsNull("productname") )
						return null;
				
					return (String) this.DataRow["productname"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["productname"] = value;
				
				this._ProductName = value;					
			}
		}
		private String _ProductName;

		public Int32 SupplierID
		{
			get
			{
				if (this.DataRow == null)
					return this._SupplierID;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.SupplierIDColumn) )
						return 0;
					return (Int32) this.DataRow[this.SupplierIDColumn];
				}
				else
				{
					if (this.DataRow.IsNull("supplierid") )
						return 0;
				
					return (Int32) this.DataRow["supplierid"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["supplierid"] = value;
				
				this._SupplierID = value;					
			}
		}
		private Int32 _SupplierID;

		public Int32 CategoryID
		{
			get
			{
				if (this.DataRow == null)
					return this._CategoryID;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.CategoryIDColumn) )
						return 0;
					return (Int32) this.DataRow[this.CategoryIDColumn];
				}
				else
				{
					if (this.DataRow.IsNull("categoryid") )
						return 0;
				
					return (Int32) this.DataRow["categoryid"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["categoryid"] = value;
				
				this._CategoryID = value;					
			}
		}
		private Int32 _CategoryID;

		public String QuantityPerUnit
		{
			get
			{
				if (this.DataRow == null)
					return this._QuantityPerUnit;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.QuantityPerUnitColumn) )
						return null;
					return (String) this.DataRow[this.QuantityPerUnitColumn];
				}
				else
				{
					if (this.DataRow.IsNull("quantityperunit") )
						return null;
				
					return (String) this.DataRow["quantityperunit"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["quantityperunit"] = value;
				
				this._QuantityPerUnit = value;					
			}
		}
		private String _QuantityPerUnit;

		public Decimal UnitPrice
		{
			get
			{
				if (this.DataRow == null)
					return this._UnitPrice;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.UnitPriceColumn) )
						return 0M;
					return (Decimal) this.DataRow[this.UnitPriceColumn];
				}
				else
				{
					if (this.DataRow.IsNull("unitprice") )
						return 0M;
				
					return (Decimal) this.DataRow["unitprice"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["unitprice"] = value;
				
				this._UnitPrice = value;					
			}
		}
		private Decimal _UnitPrice;

		public Int16 UnitsInStock
		{
			get
			{
				if (this.DataRow == null)
					return this._UnitsInStock;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.UnitsInStockColumn) )
						return 0;
					return (Int16) this.DataRow[this.UnitsInStockColumn];
				}
				else
				{
					if (this.DataRow.IsNull("unitsinstock") )
						return 0;
				
					return (Int16) this.DataRow["unitsinstock"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["unitsinstock"] = value;
				
				this._UnitsInStock = value;					
			}
		}
		private Int16 _UnitsInStock;

		public Int16 UnitsOnOrder
		{
			get
			{
				if (this.DataRow == null)
					return this._UnitsOnOrder;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.UnitsOnOrderColumn) )
						return 0;
					return (Int16) this.DataRow[this.UnitsOnOrderColumn];
				}
				else
				{
					if (this.DataRow.IsNull("unitsonorder") )
						return 0;
				
					return (Int16) this.DataRow["unitsonorder"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["unitsonorder"] = value;
				
				this._UnitsOnOrder = value;					
			}
		}
		private Int16 _UnitsOnOrder;

		public Int16 ReorderLevel
		{
			get
			{
				if (this.DataRow == null)
					return this._ReorderLevel;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.ReorderLevelColumn) )
						return 0;
					return (Int16) this.DataRow[this.ReorderLevelColumn];
				}
				else
				{
					if (this.DataRow.IsNull("reorderlevel") )
						return 0;
				
					return (Int16) this.DataRow["reorderlevel"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["reorderlevel"] = value;
				
				this._ReorderLevel = value;					
			}
		}
		private Int16 _ReorderLevel;

		public Boolean Discontinued
		{
			get
			{
				if (this.DataRow == null)
					return this._Discontinued;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.DiscontinuedColumn) )
						return false;
					return (Boolean) this.DataRow[this.DiscontinuedColumn];
				}
				else
				{
					if (this.DataRow.IsNull("discontinued") )
						return false;
				
					return (Boolean) this.DataRow["discontinued"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["discontinued"] = value;
				
				this._Discontinued = value;					
			}
		}
		private Boolean _Discontinued;

		public DateTime ReorderExpected
		{
			get
			{
				if (this.DataRow == null)
					return this._ReorderExpected;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.ReorderExpectedColumn) )
						return DateTime.MinValue;
					return (DateTime) this.DataRow[this.ReorderExpectedColumn];
				}
				else
				{
					if (this.DataRow.IsNull("reorderexpected") )
						return DateTime.MinValue;
				
					return (DateTime) this.DataRow["reorderexpected"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["reorderexpected"] = value;
				
				this._ReorderExpected = value;					
			}
		}
		private DateTime _ReorderExpected;


		// *** Column Definitions
		DataColumn ProductIDColumn;
		DataColumn ProductNameColumn;
		DataColumn SupplierIDColumn;
		DataColumn CategoryIDColumn;
		DataColumn QuantityPerUnitColumn;
		DataColumn UnitPriceColumn;
		DataColumn UnitsInStockColumn;
		DataColumn UnitsOnOrderColumn;
		DataColumn ReorderLevelColumn;
		DataColumn DiscontinuedColumn;
		DataColumn ReorderExpectedColumn;


		protected override void CreateColumns() 
		{
			ProductIDColumn = this.DataRow.Table.Columns["ProductID"];
			ProductNameColumn = this.DataRow.Table.Columns["ProductName"];
			SupplierIDColumn = this.DataRow.Table.Columns["SupplierID"];
			CategoryIDColumn = this.DataRow.Table.Columns["CategoryID"];
			QuantityPerUnitColumn = this.DataRow.Table.Columns["QuantityPerUnit"];
			UnitPriceColumn = this.DataRow.Table.Columns["UnitPrice"];
			UnitsInStockColumn = this.DataRow.Table.Columns["UnitsInStock"];
			UnitsOnOrderColumn = this.DataRow.Table.Columns["UnitsOnOrder"];
			ReorderLevelColumn = this.DataRow.Table.Columns["ReorderLevel"];
			DiscontinuedColumn = this.DataRow.Table.Columns["Discontinued"];
			ReorderExpectedColumn = this.DataRow.Table.Columns["ReorderExpected"];
			this.ColumnsCreated = true;
		}

	}

	[Serializable]
	public class SuppliersEntity : wwDataRowContainer 
	{			
		public SuppliersEntity() : base() {}
		public SuppliersEntity(DataRow Row) : base(Row) {}

		public Int32 SupplierID
		{
			get
			{
				if (this.DataRow == null)
					return this._SupplierID;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.SupplierIDColumn) )
						return 0;
					return (Int32) this.DataRow[this.SupplierIDColumn];
				}
				else
				{
					if (this.DataRow.IsNull("supplierid") )
						return 0;
				
					return (Int32) this.DataRow["supplierid"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["supplierid"] = value;
				
				this._SupplierID = value;					
			}
		}
		private Int32 _SupplierID;

		public String CompanyName
		{
			get
			{
				if (this.DataRow == null)
					return this._CompanyName;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.CompanyNameColumn) )
						return null;
					return (String) this.DataRow[this.CompanyNameColumn];
				}
				else
				{
					if (this.DataRow.IsNull("companyname") )
						return null;
				
					return (String) this.DataRow["companyname"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["companyname"] = value;
				
				this._CompanyName = value;					
			}
		}
		private String _CompanyName;

		public String ContactName
		{
			get
			{
				if (this.DataRow == null)
					return this._ContactName;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.ContactNameColumn) )
						return null;
					return (String) this.DataRow[this.ContactNameColumn];
				}
				else
				{
					if (this.DataRow.IsNull("contactname") )
						return null;
				
					return (String) this.DataRow["contactname"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["contactname"] = value;
				
				this._ContactName = value;					
			}
		}
		private String _ContactName;

		public String ContactTitle
		{
			get
			{
				if (this.DataRow == null)
					return this._ContactTitle;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.ContactTitleColumn) )
						return null;
					return (String) this.DataRow[this.ContactTitleColumn];
				}
				else
				{
					if (this.DataRow.IsNull("contacttitle") )
						return null;
				
					return (String) this.DataRow["contacttitle"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["contacttitle"] = value;
				
				this._ContactTitle = value;					
			}
		}
		private String _ContactTitle;

		public String Address
		{
			get
			{
				if (this.DataRow == null)
					return this._Address;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.AddressColumn) )
						return null;
					return (String) this.DataRow[this.AddressColumn];
				}
				else
				{
					if (this.DataRow.IsNull("address") )
						return null;
				
					return (String) this.DataRow["address"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["address"] = value;
				
				this._Address = value;					
			}
		}
		private String _Address;

		public String City
		{
			get
			{
				if (this.DataRow == null)
					return this._City;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.CityColumn) )
						return null;
					return (String) this.DataRow[this.CityColumn];
				}
				else
				{
					if (this.DataRow.IsNull("city") )
						return null;
				
					return (String) this.DataRow["city"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["city"] = value;
				
				this._City = value;					
			}
		}
		private String _City;

		public String Region
		{
			get
			{
				if (this.DataRow == null)
					return this._Region;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.RegionColumn) )
						return null;
					return (String) this.DataRow[this.RegionColumn];
				}
				else
				{
					if (this.DataRow.IsNull("region") )
						return null;
				
					return (String) this.DataRow["region"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["region"] = value;
				
				this._Region = value;					
			}
		}
		private String _Region;

		public String PostalCode
		{
			get
			{
				if (this.DataRow == null)
					return this._PostalCode;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.PostalCodeColumn) )
						return null;
					return (String) this.DataRow[this.PostalCodeColumn];
				}
				else
				{
					if (this.DataRow.IsNull("postalcode") )
						return null;
				
					return (String) this.DataRow["postalcode"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["postalcode"] = value;
				
				this._PostalCode = value;					
			}
		}
		private String _PostalCode;

		public String Country
		{
			get
			{
				if (this.DataRow == null)
					return this._Country;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.CountryColumn) )
						return null;
					return (String) this.DataRow[this.CountryColumn];
				}
				else
				{
					if (this.DataRow.IsNull("country") )
						return null;
				
					return (String) this.DataRow["country"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["country"] = value;
				
				this._Country = value;					
			}
		}
		private String _Country;

		public String Phone
		{
			get
			{
				if (this.DataRow == null)
					return this._Phone;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.PhoneColumn) )
						return null;
					return (String) this.DataRow[this.PhoneColumn];
				}
				else
				{
					if (this.DataRow.IsNull("phone") )
						return null;
				
					return (String) this.DataRow["phone"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["phone"] = value;
				
				this._Phone = value;					
			}
		}
		private String _Phone;

		public String Fax
		{
			get
			{
				if (this.DataRow == null)
					return this._Fax;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.FaxColumn) )
						return null;
					return (String) this.DataRow[this.FaxColumn];
				}
				else
				{
					if (this.DataRow.IsNull("fax") )
						return null;
				
					return (String) this.DataRow["fax"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["fax"] = value;
				
				this._Fax = value;					
			}
		}
		private String _Fax;

		public String HomePage
		{
			get
			{
				if (this.DataRow == null)
					return this._HomePage;

				if (this.UseColumns) 
                {
					if (this.DataRow.IsNull(this.HomePageColumn) )
						return null;
					return (String) this.DataRow[this.HomePageColumn];
				}
				else
				{
					if (this.DataRow.IsNull("homepage") )
						return null;
				
					return (String) this.DataRow["homepage"];
				}
			}
			set 
			{
				if (DataRow != null)
					this.DataRow["homepage"] = value;
				
				this._HomePage = value;					
			}
		}
		private String _HomePage;


		// *** Column Definitions
		DataColumn SupplierIDColumn;
		DataColumn CompanyNameColumn;
		DataColumn ContactNameColumn;
		DataColumn ContactTitleColumn;
		DataColumn AddressColumn;
		DataColumn CityColumn;
		DataColumn RegionColumn;
		DataColumn PostalCodeColumn;
		DataColumn CountryColumn;
		DataColumn PhoneColumn;
		DataColumn FaxColumn;
		DataColumn HomePageColumn;


		protected override void CreateColumns() 
		{
			SupplierIDColumn = this.DataRow.Table.Columns["SupplierID"];
			CompanyNameColumn = this.DataRow.Table.Columns["CompanyName"];
			ContactNameColumn = this.DataRow.Table.Columns["ContactName"];
			ContactTitleColumn = this.DataRow.Table.Columns["ContactTitle"];
			AddressColumn = this.DataRow.Table.Columns["Address"];
			CityColumn = this.DataRow.Table.Columns["City"];
			RegionColumn = this.DataRow.Table.Columns["Region"];
			PostalCodeColumn = this.DataRow.Table.Columns["PostalCode"];
			CountryColumn = this.DataRow.Table.Columns["Country"];
			PhoneColumn = this.DataRow.Table.Columns["Phone"];
			FaxColumn = this.DataRow.Table.Columns["Fax"];
			HomePageColumn = this.DataRow.Table.Columns["HomePage"];
			this.ColumnsCreated = true;
		}

	}

}

#if false   // Generation Configuration Settings - don't remove
<!-- Start DataRowContainersConfiguration -->
<?xml version="1.0" encoding="utf-8"?>
<EntityGenerator xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
   <FileName>DataRowGenerator.Config</FileName>
   <ConnectionString>Data Source=.\SQLEXPRESS;AttachDbFilename=C:\projects2005\Articles\DataBinding\APP_Data\AjaxAspNet_Data.mdf;Integrated Security=True;User Instance=True;</ConnectionString>
   <Tables>Categories,Customers,Products,Suppliers</Tables>
   <TablePrefix />
   <StripTablePrefixForClass>false</StripTablePrefixForClass>
   <Namespace>DataEntities</Namespace>
   <OutputFile>C:\projects2005\Articles\DataBinding\App_Code\DataEntities.cs</OutputFile>
   <EntityClassPostFix>Entity</EntityClassPostFix>
   <UseProperCaseForProperties>false</UseProperCaseForProperties>
   <UseProperCaseForClasses>false</UseProperCaseForClasses>
   <wwDataRowContainersNameSpace>Westwind.BusinessObjects</wwDataRowContainersNameSpace>
</EntityGenerator>
<!-- End DataRowContainersConfiguration -->
#endif
