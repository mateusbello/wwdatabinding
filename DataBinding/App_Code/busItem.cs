using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Westwind.BusinessObjects;
using DataEntities;

/// <summary>
/// Summary description for busItem
/// </summary>
public class busItem : wwBusiness<ProductsEntity>
{
   public busItem()
   {
      this.ConnectionString = ConfigurationManager.ConnectionStrings["AjaxAspNet"].ConnectionString;
      this.ConnectType = ServerTypes.SqlServer;
      this.NewRowBlankValues = true;

      this.Tablename = "Products";
      this.PkField = "ProductID";
      this.PkType = PkFieldTypes.intType;
   }

   public bool Validate()
   {
      this.ValidationErrors.Clear();

      if (this.Entity.UnitPrice < 0)
         this.ValidationErrors.Add("Unit price can't be smaller than 0", "txtUnitPrice");

      if (string.IsNullOrEmpty(Entity.ProductName))
         this.ValidationErrors.Add("Product Name can't be empty", "txtProductName");

      if (this.Entity.UnitsInStock < 0)
         this.ValidationErrors.Add("Units in Stock can't be less than 0", "txtUnitPrice");

      if (this.Entity.ReorderExpected > DateTime.Now.AddYears(1))
         this.ValidationErrors.Add("Expected data too far in the future", "txtReorderExpected");

      if (this.ValidationErrors.Count > 0)
         return false;

      return true;
   }

   /// <summary>
   /// Returns a list of items. ProductId and ProductName fields.
   /// </summary>
   /// <param name="Table"></param>
   /// <returns></returns>
   public int GetItemList(string Table)
   {
      return this.Execute("select ProductId, ProductName from " + this.Tablename + " order by 2", Table);
   }

}
