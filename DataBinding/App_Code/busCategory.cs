using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Westwind.BusinessObjects;
using DataEntities;

/// <summary>
/// Summary description for busItem
/// </summary>
public class busCategory : wwBusiness<CategoriesEntity>
{
    public busCategory()
    {
        this.ConnectionString = ConfigurationManager.ConnectionStrings["AjaxAspNet"].ConnectionString;
        this.ConnectType = ServerTypes.SqlServer;
        this.NewRowBlankValues = true;

        this.Tablename = "Categories";
        this.PkField = "CategoryID";
        this.PkType = PkFieldTypes.intType;
    }

    /// <summary>
    /// Return a list of categories with CategoryName and Category ID
    /// </summary>
    /// <returns></returns>
    public int GetCategoryList(string Table)
    {
        return this.Execute("select CategoryId,CategoryName from " + this.Tablename + " order by CategoryName",Table);
    }

}
