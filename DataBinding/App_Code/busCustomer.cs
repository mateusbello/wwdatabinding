using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DataEntities;

using Westwind.BusinessObjects;

/// <summary>
/// Summary description for busCustomer
/// </summary>
public class busCustomer : wwBusiness<CustomersEntity>
{
    public busCustomer()
    {
        this.ConnectionString = ConfigurationManager.ConnectionStrings["AjaxAspNet"].ConnectionString;
        this.ConnectType = ServerTypes.SqlServer;
        this.NewRowBlankValues = true;

        this.Tablename = "Customers";
        this.PkField = "CustomerId";
        this.PkType = PkFieldTypes.stringType;
    }
}
