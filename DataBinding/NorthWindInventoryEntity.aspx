<%@ Page Language="C#" AutoEventWireup="true" 
         CodeFile="NorthWindInventoryEntity.aspx.cs" 
         Inherits="NorthwindInventoryEntity" 
         EnableViewState="true" %>
<%@ Register Assembly="wwDataBinder" Namespace="MsdnMag.Web.Controls" TagPrefix="ww" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Northwind Inventory (Data)</title>
    <link href="Westwind.css" rel="stylesheet" type="text/css" />
    <style>
        .columnlabel { background: darkblue }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h1>Northwind Product List Entity Object Binding</h1>
    <small><a href="default.aspx">Home</a> | <a href="NorthwindInventoryEntity.aspx">Reload Form</a></small>
        <br />
    <br />
    <div style="padding-left:50px; width:550px" >

    <ww:wwErrorDisplay ID="ErrorDisplay" runat='server' Width="500px"
                       UserMessage="Please correct the following:" 
                       Center="true"></ww:wwErrorDisplay>
    <br />
    <table class="blackborder" style="width:550px;text-align:left;" cellpadding="5">
    <tr>
        <td style="width:125px" class="gridheader"></td>
        <td class="gridheader"><asp:DropDownList ID="txtProductId" runat="server" Width="350px" AutoPostBack="True" OnSelectedIndexChanged="txtProductId_SelectedIndexChanged">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td align="right" class="blockheader" style="width:125px">Description:</td>
        <td><asp:TextBox runat="server" ID="txtProductName" Width="350px"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="right" class="blockheader" style="width:125px">Category:</td>
        <td><asp:DropDownList runat="server" ID="txtCategoryId"  Width="350px"></asp:DropDownList></td>
    </tr>
    <tr>
        <td align="right" class="blockheader" style="width:125px">Supplier:</td>
        <td><asp:DropDownList runat="server" ID="txtSupplierId"  Width="350px"></asp:DropDownList></td>
    </tr>
    <tr>
        <td align="right" class="blockheader">Price:</td>
        <td><asp:TextBox runat="server" ID="txtUnitPrice" Width="75px"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp; &nbsp;
            Units on hand: <asp:TextBox runat="server" ID="txtUnitsInStock" Width="75px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td align="right" class="blockheader">Reorder Level:</td>
        <td><asp:TextBox runat="server" ID="txtReorderLevel" Width="75px"></asp:TextBox>&nbsp;&nbsp;&nbsp;
            Reorder Expected: <asp:TextBox runat="server" ID="txtReorderExpected" Width="75px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td align="right" class="blockheader" style="width:125px">Discontinued:</td>
        <td style="border-bottom: dashed 1px;"><asp:CheckBox runat="server" ID="chkDiscontinued" Width="350px" Text="Discontinued"  /></td>
    </tr>
    <tr >
        <td align="right" class="blockheader" style="width:125px">
            Current Time:</td>
        <td><asp:TextBox runat="server" ID="txtRightNow" Width="175px" /> <small>(Page property binding)</small></td>
    </tr>
    
    </table>
        <br />
        <asp:Button ID="btnSubmit" runat="server" Text="Save" OnClick="btnSubmit_Click" Height="36px" Width="119px" />
        <br />
    </div>
    
    </div>
        <ww:wwDataBinder ID="DataBinder" runat="server" OnValidateControl="DataBinder_ValidateControl">
            <DataBindingItems>
                <ww:wwDataBindingItem runat="server" BindingSource="Item.Entity" BindingSourceMember="ProductName"
                    ControlId="txtProductName" IsRequired="True" ErrorMessageLocation="RedTextAndIconBelow">
                </ww:wwDataBindingItem>
                <ww:wwDataBindingItem runat="server" BindingProperty="SelectedValue" BindingSource="Item.Entity"
                    BindingSourceMember="CategoryId" ControlId="txtCategoryId" UserFieldName="Category">
                </ww:wwDataBindingItem>
                <ww:wwDataBindingItem runat="server" BindingSource="Item.Entity" BindingSourceMember="UnitPrice"
                    ControlId="txtUnitPrice" DisplayFormat="{0:f2}" IsRequired="True" UserFieldName="Unit Price">
                </ww:wwDataBindingItem>
                <ww:wwDataBindingItem runat="server" BindingProperty="Checked" BindingSource="Item.Entity"
                    BindingSourceMember="Discontinued" ControlId="chkDiscontinued" UserFieldName="Discontinued">
                </ww:wwDataBindingItem>
                <ww:wwDataBindingItem runat="server" BindingSource="this" BindingSourceMember="RightNow"
                    ControlId="txtRightNow" UserFieldName="Right Now">
                </ww:wwDataBindingItem>
                <ww:wwDataBindingItem runat="server" BindingSource="Item.Entity" BindingSourceMember="UnitsInStock"
                    ControlId="txtUnitsInStock" DisplayFormat="{0:f0}" >
                </ww:wwDataBindingItem>
                <ww:wwDataBindingItem runat="server"  ControlId="txtSupplierId" 
                                       BindingSource="Item.Entity"
                                       BindingSourceMember="SupplierId" 
                                       BindingProperty="SelectedValue" >
                </ww:wwDataBindingItem>
                <ww:wwDataBindingItem runat="server" BindingSource="Item.Entity" BindingSourceMember="ReorderLevel"
                    ControlId="txtReorderLevel">
                </ww:wwDataBindingItem>
                <ww:wwDataBindingItem runat="server" BindingSource="Item.Entity" BindingSourceMember="ReorderExpected"
                    ControlId="txtReorderExpected" DisplayFormat="{0:d}" UserFieldName="Reorder Expected">
                </ww:wwDataBindingItem>
            </DataBindingItems>
        </ww:wwDataBinder>
        &nbsp;
    </form>
</body>
</html>
