<%@ Page Language="C#" AutoEventWireup="true" 
                       CodeFile="FormView.aspx.cs" Inherits="stockBinding_FormView"  
                       EnableEventValidation="false" 
                       EnableViewState="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:SqlDataSource ID="DataSource" runat="server" ConnectionString="<%$ ConnectionStrings:AjaxAspNet %>"
            DeleteCommand="DELETE FROM [Products] WHERE [ProductID] = @ProductID" 
            InsertCommand="INSERT INTO [Products] ([ProductName], [SupplierID], [CategoryID], [QuantityPerUnit], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [ReorderLevel], [Discontinued], [ReorderExpected]) VALUES (@ProductName, @SupplierID, @CategoryID, @QuantityPerUnit, @UnitPrice, @UnitsInStock, @UnitsOnOrder, @ReorderLevel, @Discontinued, @ReorderExpected)"
            UpdateCommand="UPDATE [Products] SET [ProductName] = @ProductName, [SupplierID] = @SupplierID, [CategoryID] = @CategoryID, [QuantityPerUnit] = @QuantityPerUnit, [UnitPrice] = @UnitPrice, [UnitsInStock] = @UnitsInStock, [UnitsOnOrder] = @UnitsOnOrder, [ReorderLevel] = @ReorderLevel, [Discontinued] = @Discontinued, [ReorderExpected] = @ReorderExpected WHERE [ProductID] = @ProductID"
            SelectCommand="select * from [Products] order by ProductName"
            >
            <DeleteParameters>
                <asp:Parameter Name="ProductID" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="ProductName" Type="String" />
                <asp:Parameter Name="SupplierID" Type="Int32" />
                <asp:Parameter Name="CategoryID" Type="Int32" />
                <asp:Parameter Name="QuantityPerUnit" Type="String" />
                <asp:Parameter Name="UnitPrice" Type="Decimal" />
                <asp:Parameter Name="UnitsInStock" Type="Int16" />
                <asp:Parameter Name="UnitsOnOrder" Type="Int16" />
                <asp:Parameter Name="ReorderLevel" Type="Int16" />
                <asp:Parameter Name="Discontinued" Type="Boolean" />
                <asp:Parameter Name="ReorderExpected" Type="DateTime" />
                <asp:Parameter Name="ProductID" Type="Int32" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="ProductName" Type="String" />
                <asp:Parameter Name="SupplierID" Type="Int32" />
                <asp:Parameter Name="CategoryID" Type="Int32" />
                <asp:Parameter Name="QuantityPerUnit" Type="String" />
                <asp:Parameter Name="UnitPrice" Type="Decimal" />
                <asp:Parameter Name="UnitsInStock" Type="Int16" />
                <asp:Parameter Name="UnitsOnOrder" Type="Int16" />
                <asp:Parameter Name="ReorderLevel" Type="Int16" />
                <asp:Parameter Name="Discontinued" Type="Boolean" />
                <asp:Parameter Name="ReorderExpected" Type="DateTime" />
            </InsertParameters>
        </asp:SqlDataSource>
    
    </div>
        <asp:FormView ID="FormView" runat="server" 
                      DataSourceID="DataSource" 
                      DataKeyNames="ProductID">
        <EditItemTemplate>
        <h3>EditTemplate</h3>
            <asp:TextBox runat="server" ID="txtProductName" Width="450px" Text='<%# Bind("ProductName") %>'></asp:TextBox>
            <asp:TextBox runat="server" ID="txtUnitPrice" Width="175px" Text='<%# Bind("UnitPrice") %>' On></asp:TextBox>
            Units on hand: <asp:TextBox runat="server" ID="txtUnitsInStock" Width="148px" Text='<%# Bind("UnitsInStock") %>'></asp:TextBox>
            <asp:CheckBox runat="server" ID="CheckBox1" Width="450px" Text="Discontinued" Checked='<%# Bind("Discontinued") %>'/>
            <asp:Button runat="server" Text="Save" CommandName="Update"/>
        </EditItemTemplate>
        <ItemTemplate>
        <h3>ItemTemplate</h3>
            <asp:TextBox runat="server" ID="txtProductName" Width="450px" Text='<%# Bind("ProductName") %>'></asp:TextBox>
            <asp:TextBox runat="server" ID="txtUnitPrice" Width="175px" Text='<%# Bind("UnitPrice") %>'></asp:TextBox>
            Units on hand: <asp:TextBox runat="server" ID="txtUnitsInStock" Width="148px" Text='<%# Bind("UnitsInStock") %>'></asp:TextBox>
            <asp:CheckBox runat="server" ID="chkDiscontinued" Width="450px" Text="Discontinued" Checked='<%# Bind("Discontinued") %>'/>
            <asp:Button runat="server"  CommandName="Edit" width="120px" Text="Edit"/>
        </ItemTemplate>
        </asp:FormView>
    </form>
</body>
</html>
