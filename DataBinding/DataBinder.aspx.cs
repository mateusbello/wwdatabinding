using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class DataBinder : System.Web.UI.Page
{
    protected busCustomer Customer = new busCustomer();
    public decimal SomeValue = 1210.12M;
    public DateTime SomeDate = DateTime.Now;

    protected void Page_Load(object sender, EventArgs e)
    {
        // *** Bind all controls that implement IwwDataBinder
        //this.Binder.LoadFromControls(this);

        // *** Manually add controls to bind in code
        //this.Binder.AddBinding(this.txtTime, "Text", this, "SomeDate");
        
        Customer.Load("ALFKI");

        //this.Binder.GetDataBindingItem(this.TextBox1).DataBind();

        if (!this.IsPostBack)
        {
            if (!this.Binder.DataBind())
                this.ErrorDisplay.ShowError(this.Binder.BindingErrors.ToHtml());
        }
        //else
        //    // *** Individually bind a control 'manually' 
        //    this.Binder.GetDataBindingItem(this.btnSave).DataBind();

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        this.Binder.Unbind();
        this.ErrorDisplay.ShowMessage(SomeValue.ToString() + " " + this.Customer.DataRow["CompanyName"].ToString() + "<p>" + this.Binder.BindingErrors.ToHtml());
    }
}
