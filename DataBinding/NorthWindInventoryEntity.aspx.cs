using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Westwind.BusinessObjects;
//using Westwind.Tools;
using MsdnMag.Web.Controls;

public partial class NorthwindInventoryEntity : System.Web.UI.Page
{
    public busItem Item = null;
    public busCategory Category = null;
    public busSupplier Supplier = null;
    
    public DateTime RightNow = DateTime.Now;

    bool PageError = false;

    /// <summary>
    /// Do initial DataLoading in OnInit
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
         base.OnInit(e);

         this.Item = new busItem();
         this.Category = new busCategory();
         this.Supplier = new busSupplier();

         // *** This content goes into ViewState 
         if (!this.IsPostBack)
             this.LoadLists();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.PageError)
            return;
    
        // *** We only databind on initial load
        if (!this.IsPostBack)
        {
            int FirstItemPk = (int) Item.DataSet.Tables["TItemList"].Rows[0]["ProductId"];

            if (!this.Item.Load(FirstItemPk) )
            {
                this.ErrorDisplay.ShowError(Item.ErrorMessage);
                this.Item.New();
            }

            // *** Go do it: Bind the any Binding Items to the Controls
            this.DataBinder.DataBind();
        }
    }

     protected void btnSubmit_Click(object sender, EventArgs e)
    {
        // *** Reload Item record to override only data that's changed
        if (!this.Item.Load(int.Parse(this.txtProductId.SelectedValue)))
        {
            this.ErrorDisplay.ShowMessage("Invalid product id selected");
            return;
        }
        
        // *** Now unbind Controls into the Item.Entity / Item.Entity and other bindings
        // *** Note: returns a bool value, but usually you'll check DataBinder.BindingErrors as below
        bool IsError = this.DataBinder.Unbind(this);

        if (!Item.Validate())
        {
            foreach (ValidationError Error in Item.ValidationErrors)
            {
                this.DataBinder.AddBindingError(Error.Message, Error.ControlID);
            }
        }

        // *** Manually add a binding error to a non-bound control
        if (this.txtProductId.SelectedIndex == 0)
        {
            // *** If the item isn't already bound we can create a binding
            // *** if already bound this isn't necessary
            this.DataBinder.AddBinding(this.txtProductId);

            // Assign a binding error to it
            this.DataBinder.AddBindingError("First Item in list can't be updated (manual binding)", this.txtProductId);
        }

        if (DataBinder.BindingErrors.Count > 0)
        {
            this.ErrorDisplay.Text = DataBinder.BindingErrors.ToHtml();
            return;
        }

        if (!Item.Save())
            this.ErrorDisplay.ShowError(Item.ErrorMessage, "Couldn't save Item");
        else
            this.ErrorDisplay.ShowMessage("Inventory Item has been saved.<br />Updated saved Time: " + this.RightNow.ToString());
    }

    protected void txtProductId_SelectedIndexChanged(object sender, EventArgs e)
    {
        int Pk = -1;
        int.TryParse(this.txtProductId.SelectedValue, out Pk);
        if (Pk == -1)
        {
            this.ErrorDisplay.ShowError("Invalid Selection");
            return;
        }

        // *** Rebind the item after change was made
        this.Item.Load(Pk);
        this.DataBinder.DataBind();
    }

    protected bool DataBinder_ValidateControl(wwDataBindingItem Item)
    {
        if (Item.ControlInstance == this.txtCategoryId)
        {
            DropDownList List = Item.ControlInstance as DropDownList;
            if (List.SelectedItem.Text == "Dairy Products")
            {
                // *** Add a BindingErrorMessage to the DataBindingItem
                Item.BindingErrorMessage = "Dairy Properties are bad for you (ValidateControl)";
                
                // *** Return false to tell that validation failed
                return false;
            }
        }

        return true;
    }

    protected void LoadLists()
    {
            // *** Bus Object method to load ItemList Table   
             if (this.Item.GetItemList("TItemList") < 0)
             {
                 this.ErrorDisplay.ShowError(Item.ErrorMessage, "Couldn't load items");
                 this.PageError = true;
                 return;
             }

             // *** Bus object method load Category List Table
             if (this.Category.GetCategoryList("TCategoryList") < 0)
             {
                 this.ErrorDisplay.ShowError(Item.ErrorMessage, "Couldn't load categories");
                 this.PageError = true; 
                 return;
             }
             if (this.Supplier.GetSupplierList("TSupplierList") < 0)
             {
                 this.ErrorDisplay.ShowError(Supplier.ErrorMessage, "Couldn't load suppliers");
                 this.PageError = true; 
                 return;
             }

             // *** Bind the lists
             this.txtCategoryId.DataSource = this.Category.DataSet.Tables["TCategoryList"];
             this.txtCategoryId.DataTextField = "CategoryName";
             this.txtCategoryId.DataValueField = "CategoryId";
             this.txtCategoryId.DataBind();

             this.txtProductId.DataSource = this.Item.DataSet.Tables["TItemList"];
             this.txtProductId.DataTextField = "ProductName";
             this.txtProductId.DataValueField = "ProductId";
             this.txtProductId.DataBind();

             this.txtSupplierId.DataSource = this.Supplier.DataSet.Tables["TSupplierList"];
             this.txtSupplierId.DataTextField = "CompanyName";
             this.txtSupplierId.DataValueField = "SupplierId";
             this.txtSupplierId.DataBind();
    }
}
