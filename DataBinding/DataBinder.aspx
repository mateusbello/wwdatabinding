<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DataBinder.aspx.cs" Inherits="DataBinder" trace="true" %>
<%@ Register Assembly="wwDataBinder" Namespace="MsdnMag.Web.Controls" TagPrefix="ww" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>DataBinding</title>
    <link href="Westwind.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <%# DateTime.Now %>
        <h1>
            Simple DataBinding Test</h1>
            <a href="DataBinder.aspx">Refresh Page</a>
        <br />
        <ww:wwErrorDisplay ID="ErrorDisplay" runat="server">
        </ww:wwErrorDisplay>

        <br />
        <asp:TextBox ID="txtCompany" runat="server" Height="22px" Width="400px" />
        <br />
        <asp:TextBox ID="txtName" runat="server" Height="22px" UserFieldName="Name" Width="400px"/>
        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtName"
            Display="Dynamic" EnableClientScript="False" ErrorMessage="Name must be text"
            ValueToCompare="Test"></asp:CompareValidator><br />
        &nbsp;<asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="CustomValidator"></asp:CustomValidator><br />
        <br />
        <asp:TextBox runat="server" ID="txtTime"></asp:TextBox>
        <br />
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <br />
        WebTextBox: 
             <ww:WebTextBox ID="txtNewValue" runat="server" Width="305px" 
                   BindingItem-BindingSource="Customer.DataRow" 
                   BindingItem-BindingSourceMember="Address" 
                   BindingItem-ControlId="txtNewValue" 
                   BindingItem-DisplayFormat="{0:d}">
               </ww:WebTextBox>
               
               <br />
        WebTextBox: 
        <ww:WebTextBox ID="txtName2" runat="server" 
                       BindingItem-BindingSource="Customer.DataRow" 
                       BindingItem-BindingSourceMember="ContactName" 
                       BindingItem-ControlId="txtName2"></ww:WebTextBox>
        
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" EnableViewstate="false"/><br />
        <br />
    </div>
        <asp:TextBox ID="txtSomeValue" runat="server" Height="22px" Width="400px" />
        <br />
        <br />

        <ww:wwDataBinder runat='server' ID="Binder">
        <DataBindingItems>
            <ww:wwDataBindingItem runat="server" BindingSource='Customer.DataSet.Tables[&quot;customers&quot;].Rows[0]' BindingSourceMember="PostalCode"
                ControlId="txtName">
            </ww:wwDataBindingItem>
            <ww:wwDataBindingItem runat="server" BindingSource="Customer.DataRow" BindingSourceMember="ContactName"
                ControlId="txtCompany" ErrorMessageLocation="RedTextAndIconBelow" IsRequired="True" UserFieldName="Contact Name">
            </ww:wwDataBindingItem>
            <ww:wwDataBindingItem runat="server" BindingSource="Customer.DataRow" BindingSourceMember="Address"
                ControlId="txtTime">
            </ww:wwDataBindingItem>
            <ww:wwDataBindingItem runat="server" BindingSource="Customer.DataRow" BindingSourceMember="City"
                ControlId="ErrorDisplay" >
            </ww:wwDataBindingItem>
            <ww:wwDataBindingItem runat="server" ControlId="TextBox1" BindingSource="Customer.DataRow" BindingSourceMember="City">
            </ww:wwDataBindingItem>
            <ww:wwDataBindingItem runat="server" BindingMode="OneWay"
                BindingSource="Customer.DataRow" BindingSourceMember="PostalCode" ControlId="btnSave"
                UserFieldName="Save Button">
            </ww:wwDataBindingItem>
            <ww:wwDataBindingItem runat="server" ControlId="CompareValidator1">
            </ww:wwDataBindingItem>
        </DataBindingItems>
        </ww:wwDataBinder>
    
        
    </form>
</body>
</html>
