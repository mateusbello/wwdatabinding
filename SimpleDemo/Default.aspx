﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1.Default" %>
<%@ Register TagPrefix="ww" Namespace="MsdnMag.Web.Controls" Assembly="wwDataBinder" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:DropDownList runat="server" ID="drop1">
        
            <asp:ListItem Text="a" Value="b"></asp:ListItem>
                 <asp:ListItem Text="c" Value="d"></asp:ListItem>
             <asp:ListItem Text="e" Value="f"></asp:ListItem>
        </asp:DropDownList>
        
        <asp:Button runat="server" Text="tt" OnClick="btnClick" />
        
        <asp:RadioButton runat="server" ID="asd" Text="sasda" Checked="true"/>
    </div>
        
        <ww:wwDataBinder runat='server' ID="Binder">
        <DataBindingItems>
           
            <ww:wwDataBindingItem runat="server"  ControlId="drop1" BindingMode="TwoWay"  BindingSource="Customer" BindingSourceMember="Name">
            </ww:wwDataBindingItem>
             <ww:wwDataBindingItem runat="server" ControlId="TextBox1" BindingMode="TwoWay" BindingSource="Customer" BindingSourceMember="value">
            </ww:wwDataBindingItem>
             <ww:wwDataBindingItem runat="server" ControlId="asd" BindingProperty="Checked" BindingMode="TwoWay" BindingSource="Customer" BindingSourceMember="istrue">
            </ww:wwDataBindingItem>
           
        </DataBindingItems>
        </ww:wwDataBinder>
    </form>
</body>
</html>
