﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Default : System.Web.UI.Page
    {
        
        protected Customer Customer
        {
            get
            {
                if (ViewState["Customer"] == null)
                {
                    return new Customer();
                }
                else
                {
                    return (Customer)ViewState["Customer"];
                   
                }
            }

            set
            {
                ViewState["Customer"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Customer  cust = new Customer();
                cust.aaa = "asdasd";
                Customer = cust;

                this.Binder.DataBind();
                //this.ErrorDisplay.ShowError(this.Binder.BindingErrors.ToHtml());
            }
        }

        protected void btnClick(object sender, EventArgs e)
        {
          var a =  this.Binder.Unbind();
        }
    }
}