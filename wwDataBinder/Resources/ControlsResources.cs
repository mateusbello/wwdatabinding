using System;
using System.Collections.Generic;
using System.Text;

using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("MsdnMag.Web.Controls.Resources.wwHoverPanel.js", "text/javascript")]
[assembly: WebResource("MsdnMag.Web.Controls.Resources.warning.gif", "image/gif")]
[assembly: WebResource("MsdnMag.Web.Controls.Resources.info.gif", "image/gif")]

namespace MsdnMag.Web.Controls
{
    /// <summary>
    /// Class is used as to consolidate access to resources
    /// </summary>
    public class ControlsResources
    {
        public const string HOVERPANEL_SCRIPT_RESOURCE = "MsdnMag.Web.Controls.Resources.wwHoverPanel.js";
        public const string INFO_ICON_RESOURCE = "MsdnMag.Web.Controls.Resources.info.gif";
        public const string WARNING_ICON_RESOURCE = "MsdnMag.Web.Controls.Resources.warning.gif";
    }
}
