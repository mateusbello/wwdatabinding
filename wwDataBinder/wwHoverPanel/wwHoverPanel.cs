//#define wwWebControls
/*
 **************************************************************
 * wwHoverPanel Class
 **************************************************************
 *  Author: Rick Strahl 
 *          (c) West Wind Technologies
 *          http://www.west-wind.com/
 * 
 * Created: 1/26/2006 
 * 
 * This ASP.NET Control class provides a number of AJAX
 * functionalities for ASP.NET pages.
 * 
 * Hover Windows:
 * You can specify a URL and use the HoverWindow option
 * to automatically pop up a window at the current mouse 
 * location. Handles auto positioning, delayed display,
 * and basic display management of the panel displayed.
 * 
 * Callback Method Calls:
 * Allows you to create plain methods on the current Page
 * marked up with [CallbackMethod] to make server calls.
 * In client script you can use MyControl_CallMethod()
 * to call the server and return the result from the method
 * in JSON format.
 * 
 * Callback Url Results:
 * Allows you to call another URL and callback to a specified
 * client script handler method. The values returned from this
 * routine are pure strings.
 * 
 * Source Dependencies:
 * JSONSerializer.cs
 * wwUtils.cs
 * 
 * Acknowledgements:
 * This code borrows concepts from Jason Diamonds MyAjax.NET
 * also known as Anthem:
 * http://sourceforge.net/mailarchive/forum.php?forum=anthem-dot-net-devel
 * 
 * The original JavaScript JSON deserialization code file is based on:
 *     copyright: '(c)2005 JSON.org',
 *       license: 'http://www.crockford.com/JSON/license.html',
 * 
 * License:
 * Free without any restrictions whatsoever
 * 
 * I only ask that if you make changes that you think are useful
 * you let us know and post a message at:
 * 
 * http://www.west-wind.com/wwThreads/default.asp?forum=West+Wind+Web+Tools+for+.NET
 * 
 * Version History:
 * Version 1.0 
 * -----------
 * * Release
 * 
 * Version 1.1 (2/23/2006)
 * ----------- 
 * * Containership Issues Fixed
 *   Fixed containership naming issue where the control wouldn't
 *   work properly hosted in a container or master page. Fixed by
 *   providing both ControlId and BaseControlId properties to the 
 *   Client Context object. BaseControlId is the ID of the control
 *   where the ControlId is the panel's ASP.NET generated ClientID.
 *   For simplicity the JSON class generated is named like the ID 
 *   not with the full cryptic ASP.NET ControlName as are the Context
 *   objects. This has the slight tradeoff that you have to make sure
 *   that the ID is unique for the page.
 * 
 * * AutoAdjustWindow  Behavior fixed
 *   Improved handling of AutoAdjustWindow behavior which tries
 *   to keep content that's scrolling off the bottom visible by
 *   raising the Panel higher up on Hover Panel operation.
 * 
 * * Aborted Hover Navigation handling fixed
 *   If you hover over a link (or whatever fires your hover request)
 *   and you HidePanel (typically a mouse out) the control now aborts
 *   the pending request and doesn't display the panel. Previously 
 *   hover operations would complete and show the content at the top
 *   of the form which was, uh, less than ideal.
 **************************************************************  
*/
#define DotNet20

using System;
using System.Text;
using System.Data;
using System.Globalization;

using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Design;
using System.Collections;

using System.Web;
using System.Collections.Specialized;
using System.Reflection;
using System.IO;

[assembly: WebResource("Westwind.Web.Controls.Resources.wwHoverPanel.js", "text/javascript")]

namespace Westwind.Web.Controls
{
    /// <summary>
    /// The wwHoverPanel class provides an easy to use base AJAX control that allows
    /// quick access to server side resources from within a client side Web page. The
    /// Control provides the following features:
    /// 
    /// HoverPanel Hover Windows
    /// Call a URL to retrieve an Html Response and display it at the current
    /// mouse position as a hover window that goes away when mousing out.
    /// 
    /// Server Page Method Callbacks using two-way JSON Serialization
    /// Call server side methods on the server page. Methods need to be marked up
    /// with a [CallbackMethod] Attribute and can simply accept and return many
    /// common data types. Simple types, hierarchical objects, arrays and IList
    /// based classes are supported for two-way transfers. DataSets/DataTables/DataRows
    /// are support for downloading only at this time.
    /// 
    /// External Page Callbacks
    /// Allows easily calling back to other pages in the Web site and return the result
    /// as a string to the client. Literally can be done with one line of code and hooking
    /// up an event handler.
    /// </summary>
    [ToolboxBitmap(typeof(Panel)), DefaultProperty("Url"),
    ToolboxData("<{0}:wwHoverPanel runat='server' style='width:450px;background:white;display:none;'></{0}:wwHoverPanel>")]
    public class wwHoverPanel : System.Web.UI.WebControls.Panel
    {
        public const string JAVASCRIPT_RESOURCE = "Westwind.Web.Controls.Resources.wwHoverPanel.js";

        /// <summary>
        /// The client script event handler function called when the remote call 
        /// returns. Receives a result string parameter.
        /// <seealso>Class wwHoverPanel</seealso>
        /// </summary>
        [Description("The client script event handler function called when the remote call returns. Receives a result string parameter."),
        DefaultValue(""), Category("HoverPanel")]
        public string ClientEventHandler
        {
            get
            {
                return _ClientEventHandler;
            }
            set
            {
                _ClientEventHandler = value;
            }
        }
        private string _ClientEventHandler = "";

        /// <summary>
        /// The Url to hit on the server for the callback to return the result. Note: Not used when doing a MethodCallback
        /// </summary>
        [Description("The Url to hit on the server for the callback to return the result."),
        DefaultValue(""), Category("HoverPanel")]
        public string ServerUrl
        {
            get
            {
                return _ServerUrl;
            }
            set
            {
                _ServerUrl = value;
            }
        }
        private string _ServerUrl = "";

        /// <summary>
        /// Determines if the navigation is delayed by a hesitation. Useful for link hovering.
        /// </summary>
        [Description("Determines if the navigation is delayed by a hesitation. Useful for link hovering.")]
        [DefaultValue(0), Category("HoverPanel")]
        public int NavigateDelay
        {
            get
            {
                return _NavigateDelay;
            }
            set
            {
                _NavigateDelay = value;
            }
        }
        private int _NavigateDelay = 0;

        /// <summary>
        /// Determines whether this request is a callback
        /// </summary>
        [Browsable(false)]
        public bool IsCallback
        {
            get
            {
                if (_IsCallback == 0)
                {
                    string Id = this.Context.Request.Params["__WWEVENTCALLBACK"];
                    if (Id != null && Id == this.ID)
                    {
                        _IsCallback = 2;
                        return true;
                    }
                    _IsCallback = 1;
                    return false;
                }
                else
                {
                    if (_IsCallback == 2)
                        return true;
                    return false;
                }
            }
        }
        private int _IsCallback = 0;

        /// <summary>
        /// Determines the how the event is handled  on the callback request. ShowHtmlMousePosition shows the result in a window. CallEventHandler fires the specified script function.
        /// </summary>
        [Description("Determines the how the event is handled  on the callback request. ShowHtmlMousePosition shows the result in a window. CallEventHandler fires the specified script function."),
       DefaultValue(HoverEventHandlerModes.ShowHtmlAtMousePosition), Category("HoverPanel")]
        public HoverEventHandlerModes EventHandlerMode
        {
            get
            {
                return _EventHandlerMode;
            }
            set
            {
                _EventHandlerMode = value;
            }
        }
        private HoverEventHandlerModes _EventHandlerMode = HoverEventHandlerModes.ShowHtmlAtMousePosition;

        /// <summary>
        /// if set tries to move up the window if it's too low to fit content. This setting can cause problems with very large content.
        /// </summary>
        [Description("If set tries to move up the window if it's too low to fit content. This setting can cause problems with very large content."),
       DefaultValue(true), Category("HoverPanel")]
        public bool AdjustWindowPosition
        {
            get
            {
                return _AdjustWindowPosition;
            }
            set
            {
                _AdjustWindowPosition = value;
            }
        }
        private bool _AdjustWindowPosition = false;

        [Description("Optional Opacity level in full percentage points for the panel background. Supported only in Mozilla and IE browsers. The value is given as fractional percentage."),
        Category("HoverPanel"), DefaultValue(1.0)]
        public decimal PanelOpacity
        {
            get { return _PanelOpacity; }
            set { _PanelOpacity = value; }
        }
        private decimal _PanelOpacity = 1.0M;

        [Description("Optionally used to specify a shadow below the panel. If 0 no shadow is created. If greater than 0 the panel is rendered."),
        Category("HoverPanel"), DefaultValue(0)]
        public int PanelShadowOffset
        {
            get { return _PanelShadowOffset; }
            set { _PanelShadowOffset = value; }
        }
        private int _PanelShadowOffset = 0;

        [Description("The opacity of the Panel's shadow if PanelShadoOffset is set."),
        Category("HoverPanel"), DefaultValue(.25)]
        public decimal PanelShadowOpacity
        {
            get { return _PanelShadowOpacity; }
            set { _PanelShadowOpacity = value; }
        }
        private decimal _PanelShadowOpacity = .25M;



        /// <summary>
        /// If true causes the page to post back all form variables.
        /// </summary>
        [Description("If true causes the page to post back all form variables."), DefaultValue(PostBackModes.GET), Category("HoverPanel")]
        public PostBackModes PostBackMode
        {
            get
            {
                return _PostBackFormData;
            }
            set
            {
                _PostBackFormData = value;
            }
        }
        private PostBackModes _PostBackFormData = PostBackModes.GET;

        /// <summary>
        /// The name of the form from which values are posted back to the server. Note only a single form's 
        /// values can be posted back!
        /// </summary>
        [Description("If PostBackData is set, this variable determines which form is posted back."), DefaultValue(""), Category("HoverPanel")]
        public string PostBackFormName
        {
            get
            {
                return _PostBackFormName;
            }
            set
            {
                _PostBackFormName = value;
            }
        }
        private string _PostBackFormName = "";


        /// <summary>
        /// Determines how the JavasScript code gets embedded into the page.
        /// </summary>
        [Description("Determines how the JavaScript code gets emedded into the page."),
        DefaultValue(JavaScriptCodeLocationTypes.EmbeddedInPage), Category("HoverPanel")]
        public JavaScriptCodeLocationTypes ScriptLocationType
        {
            get { return _ScriptLocationType; }
            set { _ScriptLocationType = value; }
        }
        private JavaScriptCodeLocationTypes _ScriptLocationType = JavaScriptCodeLocationTypes.EmbeddedInPage;


        /// <summary>
        /// Determines the location of thte JavaScript support file if ScriptLocationType is set to ExternalFile
        /// </summary>
        [Description("Determines the location of thte JavaScript support file if ScriptLocationType is set to ExternalFile"),
        DefaultValue("~/wwHoverPanel.js"), Category("HoverPanel")]
        public string ScriptLocation
        {
            get { return _ScriptLocation; }
            set { _ScriptLocation = value; }
        }
        private string _ScriptLocation = "~/wwHoverPanel.js";


        /// <summary>
        /// Override to force simple IDs all around
        /// </summary>
        public override string UniqueID
        {
            get
            {
                return this.ID;
            }
        }
        /// <summary>
        /// Override to force simple IDs all around
        /// </summary>
        public override string ClientID
        {
            get
            {
                return this.ID;
            }
        }


        protected override void OnLoad(EventArgs e)
        {
            if (!this.IsCallback)
                return;

            HttpContext.Current.Response.Expires = -1;
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);

            this.ProcessCallbackMethodCall();
        }

        /// <summary>
        /// This method just builds the various JavaScript blocks as strings
        /// and assigns them to the ClientScript object.
        //
        /// MouseEvents to the panel to show/hide the panel on mouse out 
        /// operations.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {

            if (!this.IsCallback)
            {
                this.GenerateControlSpecificJavaScript();

                if (this.ScriptLocationType == JavaScriptCodeLocationTypes.WebResource)
                {
                    string Script = this.Page.ClientScript.GetWebResourceUrl(typeof(wwHoverPanel), JAVASCRIPT_RESOURCE);
                    this.Page.ClientScript.RegisterClientScriptInclude("wwHoverPanel", Script);
                }
                else if (this.ScriptLocationType == JavaScriptCodeLocationTypes.ExternalFile)
                {
                    this.Page.ClientScript.RegisterClientScriptInclude("wwHoverPanel", this.ResolveUrl(this.ScriptLocation));
                }
                else if (this.ScriptLocationType == JavaScriptCodeLocationTypes.EmbeddedInPage)
                {
                    // *** Load the wwHoverPanel.js file from Resources for embedding
                    Stream st = Assembly.GetExecutingAssembly().GetManifestResourceStream(JAVASCRIPT_RESOURCE);
                    StreamReader sr = new StreamReader(st);

#if DotNet20
                    this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "wwHoverPanel_Generic", sr.ReadToEnd(), true);
#else
            this.Page.RegisterClientScriptBlock("GenericCode", "<script type='text/javascript'>" + sr.ReadToEnd() + "</script>");
#endif

                }

                // *** On HoverPanel operation we need to hide the window when we move off
                if (this.EventHandlerMode == HoverEventHandlerModes.ShowHtmlAtMousePosition)
                {
                    this.Attributes.Add("onmouseover", "ShowPanel('" + this.ID + "');");
                    this.Attributes.Add("onmouseout", "HidePanel('" + this.ID + "');");
                }
                else if (this.EventHandlerMode == HoverEventHandlerModes.ShowIFrameAtMousePosition ||
                         this.EventHandlerMode == HoverEventHandlerModes.ShowIFrameInPanel)
                {
                    LiteralControl Ctl = new LiteralControl();
                    Ctl.Text = "<iframe id='" + this.ClientID + "_IFrame' frameborder='0' width='" + this.Width.ToString() + "' height='" + this.Height.ToString() + "'></iframe>";
                    this.Controls.Add(Ctl);
                }

                if (this.PanelOpacity > -1 && this.PanelOpacity < 100)
                {
                    // *** Old behavior was in full percentage
                    if (this.PanelOpacity > 1)
                        this.PanelOpacity = this.PanelOpacity / 100;

                    if (this.Context.Request.Browser.ActiveXControls)
                        this.Style.Add("filter", "alpha(opacity='" + (this.PanelOpacity * 100).ToString() + "')");
                    else
                        this.Style.Add("opacity", this.PanelOpacity.ToString());
                }
            }
        }


        /// <summary>
        /// Generates the ControlSpecific JavaScript. This script is safe to
        /// allow multiple callbacks to run simultaneously.
        /// </summary>
        private void GenerateControlSpecificJavaScript()
        {
            // *** Figure out the initial URL we're going to 
            // *** Either it's the provided URL from the control or 
            // *** we're posting back to the current page
            string Url = null;
            if (this.ServerUrl == null || this.ServerUrl == "")
                Url = this.Context.Request.Path;
            else
                Url = this.ResolveUrl(this.ServerUrl);

            Uri ExistingUrl = this.Context.Request.Url;

            // *** Must fix up URL into fully qualified URL for XmlHttp
            if (!this.ServerUrl.ToLower().StartsWith("http"))
                Url = ExistingUrl.Scheme + "://" + ExistingUrl.Authority + Url;

            string CallbackHandler = this.ClientEventHandler;
            if (CallbackHandler == "")
            {
                CallbackHandler = "null";
            }

            string StartupCode =
@"
var " + this.ID + @"_Url = '" + Url + @"';
var " + this.ID + @"_Context = new CallbackContext(" + this.ID + @"_Url," + ((this.EventHandlerMode == HoverEventHandlerModes.CallExternalPage) ? CallbackHandler : this.ID + "_HandleCallback") + @",'" + this.ID + "'," + this.NavigateDelay.ToString() + ",'" + this.PostBackMode.ToString() + "','" + this.EventHandlerMode.ToString() + "','" + this.PostBackFormName + @"'," + this.AdjustWindowPosition.ToString().ToLower() + @"," + this.PanelShadowOffset.ToString() + "," + this.PanelShadowOpacity.ToString() + "," + this.PanelOpacity.ToString() + @");
function " + this.ID + @"_HandleCallback()
{
    HandleCallback('" + this.ID + @"');
}
";

#if DotNet20
            //this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), this.ID + "_STARTUP", StartupCode,true);
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), this.ID + "_STARTUP", StartupCode, true);
#else
            this.Page.RegisterStartupScript(this.ID + "_STARTUP", StartupCode);
#endif


            if (this.EventHandlerMode == HoverEventHandlerModes.CallPageMethod)
                this.GenerateClassWrapperForCallbackMethods();

        }

        /// <summary>
        /// Handles page callbacks for either plain callbacks (raw string responses)
        /// or Method Callbacks which are routed to the appropriate methods
        /// </summary>
        private void ProcessCallbackMethodCall()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string ParmCount = Request.Params["CallbackParmCount"];
            if (ParmCount == null)
                return;

            int Parameters = -1;
            int.TryParse(ParmCount, out Parameters);

            if (Parameters < 0)
                return;

            string Method = Request.Params["CallbackMethod"];

            // *** Pick up each of the encoded ParmX parameters
            ArrayList ParameterList = new ArrayList();
            for (int i = 1; i <= 4; i++)
            {
                object Parm = Request.Params["Parm" + i.ToString()];
                if (Parm == null)
                    break;
                ParameterList.Add(Parm);
            }

            if (ParameterList.Count == 0)
                ParameterList = null;

            object Result = null;
            string StringResult = null;

            try
            {
                // *** Finally: Ready to execute the method
                Result = this.ExecuteMethod(Method, ParameterList);
            }
            catch (Exception ex)
            {
                Exception ActiveException = null;
                if (ex.InnerException != null)
                    ActiveException = ex.InnerException;
                else
                    ActiveException = ex;

                wwHoverPanelException Error = new wwHoverPanelException();
                Error.Message = ActiveException.Message;
                Error.IsCallbackError = true;

                JSONSerializer Serializer = new JSONSerializer();
                StringResult = Serializer.Serialize(Error);
            }

            if (StringResult == null)
            {

                StringBuilder sb = new StringBuilder();
                try
                {
                    JSONSerializer Serializer = new JSONSerializer();
                    StringResult = Serializer.Serialize(Result);
                }
                catch (Exception ex)
                {
                    wwHoverPanelException Error = new wwHoverPanelException();
                    Error.Message = ex.Message;
                    Error.IsCallbackError = true;

                    JSONSerializer Serializer = new JSONSerializer();
                    StringResult = Serializer.Serialize(Error);
                }
            }

            HttpContext.Current.Response.Write(StringResult);
            HttpContext.Current.Response.End();
        }


        /// <summary>
        /// Executes the requested method. converts the String parameters
        /// to the proper types for execution.
        /// </summary>
        /// <param name="Method"></param>
        /// <param name="ParameterList"></param>
        /// <returns></returns>
        private object ExecuteMethod(string Method, ArrayList ParameterList)
        {
            object Result = null;
            object TPage = this.Page;

            object[] AdjustedParms = null;

            if (ParameterList != null && ParameterList.Count != 0)
            {
                object[] Parms = null;
                Parms = ParameterList.ToArray(typeof(object)) as object[];

                Type PageType = TPage.GetType();
                MethodInfo MI = PageType.GetMethod(Method, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                if (MI == null)
                    throw new ApplicationException("Invalid Server Method.");

                object[] Methods = MI.GetCustomAttributes(typeof(CallbackMethodAttribute), false);
                if (Methods.Length < 1)
                    throw new ApplicationException("Server method is not accessible due to missing CallbackMethod Attribute");

                ParameterInfo[] Parameters = MI.GetParameters();

                JSONSerializer Serializer = new JSONSerializer();

                int Count = 0;
                AdjustedParms = new object[Parms.Length];
                foreach (ParameterInfo Parameter in Parameters)
                {
                    AdjustedParms[Count] = Serializer.Deserialize(Parms[Count] as string, Parameter.ParameterType);
                    //AdjustedParms[Count] = wwUtils.StringToTypedValue(Parms[Count] as string, 
                    //                                                  Parameter.ParameterType);
                    Count++;
                }
            }
            Result = TPage.GetType().GetMethod(Method, BindingFlags.Instance |
                                                       BindingFlags.Public | BindingFlags.NonPublic).Invoke(TPage, AdjustedParms);

            return Result;
        }

        /// <summary>
        /// Creates the JavaScript client side object that matches the 
        /// server side method signature. The JScript function maps
        /// to a CallMethod() call on the client.
        /// </summary>
        private void GenerateClassWrapperForCallbackMethods()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("var " + this.ID + " = \r\n{");

            MethodInfo[] Methods = this.Page.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            foreach (MethodInfo Method in Methods)
            {
                if (Method.GetCustomAttributes(typeof(CallbackMethodAttribute), false).Length > 0)
                {
                    sb.Append("\r\n\t" + Method.Name + ": function " + "(");

                    string ParameterList = "";
                    foreach (ParameterInfo Parm in Method.GetParameters())
                    {
                        ParameterList += Parm.Name + ",";
                    }
                    sb.Append(ParameterList + "CallbackEventHandler)");

                    sb.Append(
@"
    {
        return CallMethod('" + this.ID + "','" + Method.Name + "'," +
                ParameterList + @"CallbackEventHandler);
    },");
                }
            }

            if (sb.Length > 0)
                sb.Length--; // strip trailing ,

            // *** End of class
            sb.Append("\r\n};\r\n");

#if DotNet20
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), this.ID + "_ClassStubs", sb.ToString(), true);
#else
            this.Page.RegisterStartupScript(this.ID + "_STARTUP", sb.ToString());
#endif

        }

        /// <summary>
        /// Returns an Event Callback reference string that can be used in Client
        /// script to initiate a callback request. 
        /// </summary>
        /// <param name="QueryStringExpression">
        /// An expression that is evaluated in script code and embedded as the second parameter.
        /// The value of this second parameter is interpreted as a QueryString to the URL that
        /// is fired in response to the request to the server.
        /// 
        /// This expression can be a static string or any value or expression that is in scope
        /// at the time of calling the event method. The expression must evaluate to a string
        ///  
        /// Example: 
        /// string GetCallbackEventReference("'CustomerId=' + this.forms[0].txtCustomerId.value + "'");
        ///  
        /// A callback event reference result looks like this:
        /// 
        /// ControlID_StartCallback(event,'CustomerId=_12312')
        /// </param>
        /// <returns></returns>
        public string GetCallbackEventReference(string QueryStringExpression)
        {
            if (QueryStringExpression == null)
                return "StartCallback('" + this.ID + "',event)";

            return "StartCallback('" + this.ID + "',event,'" + QueryStringExpression + "')";
        }


        ///// <summary>
        ///// Override render to support the shadow panel. Otherwise default panel rendering
        ///// </summary>
        ///// <param name="writer"></param>
        //protected override void Render(HtmlTextWriter writer)
        //{
        //    if (this.PanelShadowOffset != 0)
        //    {
        //        string opacity = "";
        //        if (Context.Request.Browser.ActiveXControls)
        //            opacity = "filter:alpha(opacity='20');";
        //        else
        //            opacity = "opacity:.2;";
        //        writer.Write("<div id='" + this.ClientID + "_Shadow' style=\"background:black;width:400px;height:200px;display:none;"  + opacity + "\"></div>\r\n");
        //    }

        //    base.Render(writer);
        //}
    }

    public enum HoverEventHandlerModes
    {

        /// <summary>
        /// Displays a hover window at the current mouse position. Calls a URL 
        /// specified in the ServerUrl property when the call is initiated. The call 
        /// initiation can add an additional queryString to specify 'parameters' for 
        /// the request.
        /// <seealso>Enumeration HoverEventHandlerModes</seealso>
        /// </summary>
        ShowHtmlAtMousePosition,
        /// <summary>
        /// Shows the result of the URL in the panel. Works like ShowHtmlInPanel
        /// except that the panel is not moved when the callback completes.
        /// </summary>
        ShowHtmlInPanel,
        /// <summary>
        /// Displays a URL in an IFRAME which is independent of the
        /// current page.
        /// </summary>
        ShowIFrameAtMousePosition,
        /// <summary>
        /// Shows an IFRAME in a panel
        /// </summary>
        ShowIFrameInPanel,
        /// <summary>
        /// Calls a server method on the current ASPX page using standard .NET 
        /// parameter and result type signatures. Values are marshalled as JSON 
        /// messages.
        /// <seealso>Enumeration HoverEventHandlerModes</seealso>
        /// </summary>
        CallPageMethod,
        /// <summary>
        /// Calls an external Page and returns the HTML result into the 
        /// ClientEventHandler specified for the control. This is a really high level 
        /// mechanism.
        /// <seealso>Enumeration HoverEventHandlerModes</seealso>
        /// </summary>
        CallExternalPage
    }

    /// <summary>
    /// Identifier Attribute to be used on Callback methods. Signals
    /// parser that the method is allowed to be executed remotely
    /// </summary>
    public class CallbackMethodAttribute : Attribute
    {
    }

    public enum PostBackModes
    {
        /// <summary>
        /// No POST data is posted back to the server
        /// </summary>
        GET,
        /// <summary>
        /// Only standard POST data is posted back - ASP.NET Post stuff left out
        /// </summary>
        POST,
        /// <summary>
        /// Posts back POST data but skips ViewState and EventTargets
        /// </summary>
        POSTNoViewstate,
        /// <summary>
        /// Posts only the method parameters and nothing else
        /// </summary>
        POSTMethodParametersOnly
    }

    public enum JavaScriptCodeLocationTypes
    {
        /// <summary>
        /// Causes the Javascript code to be embedded into the page on every 
        /// generation. Fully self-contained.
        /// <seealso>Enumeration JavaScriptCodeLocationTypes</seealso>
        /// </summary>
        EmbeddedInPage,
        /// <summary>
        /// Keeps the .js file as an external file in the Web application. If this is 
        /// set you should set the &lt;&lt;%= TopicLink([ScriptLocation],[_1Q01F9K4D]) 
        /// %&gt;&gt; Property to point at the location of the file.
        /// 
        /// This option requires that you deploy the .js file with your application.
        /// <seealso>Enumeration JavaScriptCodeLocationTypes</seealso>
        /// </summary>
        ExternalFile,
        /// <summary>
        /// ASP.NET 2.0 option to generate a WebResource.axd call that feeds the .js 
        /// file to the client.
        /// <seealso>Enumeration JavaScriptCodeLocationTypes</seealso>
        /// </summary>
        WebResource
    }

    /// <summary>
    /// Special return type used to indicate that an exception was
    /// fired on the server. This object is JSON serialized and the
    /// client can check for Result.IsCallbackError to see if a 
    /// a failure occured on the server.
    /// </summary>
    [Serializable]
    public class wwHoverPanelException
    {
        public bool IsCallbackError = true;
        public string Message = "";
    }
}
