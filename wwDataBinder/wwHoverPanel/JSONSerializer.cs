using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;

using System.Text;
using System.Globalization;
using System.Reflection;
using System.Collections;
using System.IO;
using System.Collections.Specialized;

using Westwind.Tools;

namespace Westwind.Web.Controls
{
    
    
    /// <summary>
    /// Summary description for JSONSerializer
    /// </summary>
    public class JSONSerializer
    {
        /// <summary>
        /// Serializes a value into a JSON compatible string.
        /// 
        /// The serializer supports:
        /// &lt;&lt;ul&gt;&gt;
        /// &lt;&lt;li&gt;&gt; All simple types
        /// &lt;&lt;li&gt;&gt; POCO objects and Hierarchical POCO objects
        /// &lt;&lt;li&gt;&gt; Arrays
        /// &lt;&lt;li&gt;&gt; IList based collections
        /// &lt;&lt;li&gt;&gt; DataSet
        /// &lt;&lt;li&gt;&gt; DataTable
        /// &lt;&lt;li&gt;&gt; DataRow
        /// &lt;&lt;/ul&gt;&gt;
        /// <seealso>Class JSONSerializer</seealso>
        /// </summary>
        /// <param name="Value">
        /// The strongly typed value to parse
        /// </param>
        /// <returns>string</returns>
        public string Serialize(object Value)
        {
            StringBuilder sb = new StringBuilder();
            WriteValue(sb, Value);
            return sb.ToString();
        }

        /// <summary>
        /// Takes a JSON string and attempts to create a .NET object from this 
        /// structure. An input type is required and any type that is serialized to 
        /// must support a parameterless constructor.
        /// 
        /// The de-serializer instantiates each object and runs through the properties
        /// 
        /// The deserializer supports
        /// &lt;&lt;ul&gt;&gt;
        /// &lt;&lt;li&gt;&gt; All simple types
        /// &lt;&lt;li&gt;&gt; Most POCO objects and Hierarchical POCO objects
        /// &lt;&lt;li&gt;&gt; Arrays and Object Arrays
        /// &lt;&lt;li&gt;&gt; IList based collections
        /// &lt;&lt;/ul&gt;&gt;
        /// 
        /// Note that the deserializer doesn't support DataSets/Tables/Rows like the 
        /// serializer as there's no type information available from the client to 
        /// create these objects on the fly.
        /// <seealso>Class JSONSerializer</seealso>
        /// </summary>
        /// <param name="JSONText">
        /// A string of JSON text passed from the client.
        /// </param>
        /// <param name="ValueType">
        /// The type of the object that is to be created from the JSON text.
        /// </param>
        /// <returns>The parsed object or null on failure. An exception is thrown if the type cannot be created</returns>
        public object Deserialize(string JSONText, Type ValueType)
        {
            return this.ParseValueString(JSONText,ValueType) as object;
        }

        
        #region Serialization Methods
        // This serioalization code is based on Jason Diamonds JSON parsing 
        // routines part of MyAjax.NET (aka Anthem).
        
        public void WriteValue(StringBuilder sb, object val)
        {
            if (val == null || val == System.DBNull.Value)
            {
                sb.Append("null");
            }
            else if (val is string || val is Guid)
            {
                WriteString(sb, val.ToString());
            }
            else if (val is bool)
            {
                sb.Append(val.ToString().ToLower());
            }
            else if (val is double ||
                val is float ||
                val is long ||
                val is int ||
                val is short ||
                val is byte ||
                val is decimal)
            {
                sb.Append(val);
            }
            else if (val.GetType().IsEnum)
            {
                sb.Append((int)val);
            }
            else if (val is DateTime)
            {
                sb.Append("new Date(\"");
                sb.Append(((DateTime)val).ToString("U") + " UTC");
                //sb.Append(((DateTime)val).ToString("MMMM, d yyyy HH:mm:ss", new CultureInfo("en-US", false).DateTimeFormat));
                sb.Append("\")");
            }
            else if (val is DataSet)
            {
                WriteDataSet(sb, val as DataSet);
            }
            else if (val is DataTable)
            {
                WriteDataTable(sb, val as DataTable);
            }
            else if (val is DataRow)
            {
                WriteDataRow(sb, val as DataRow);
            }
            else if (val is IEnumerable)
            {
                WriteEnumerable(sb, val as IEnumerable);
            }
            else
            {
                WriteObject(sb, val);
            }
        }

        void WriteString(StringBuilder sb, string s)
        {
            sb.Append("\"");
            foreach (char c in s)
            {
                switch (c)
                {
                    case '\"':
                        sb.Append("\\\"");
                        break;
                    case '\\':
                        sb.Append("\\\\");
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    default:
                        int i = (int)c;
                        if (i < 32 || i > 127)
                        {
                            sb.AppendFormat("\\u{0:X04}", i);
                        }
                        else
                        {
                            sb.Append(c);
                        }
                        break;
                }
            }
            sb.Append("\"");
        }

        void WriteDataSet(StringBuilder sb, DataSet ds)
        {
            sb.Append("{\"Tables\":{");
            foreach (DataTable table in ds.Tables)
            {
                sb.AppendFormat("\"{0}\":", table.TableName);
                WriteDataTable(sb, table);
                sb.Append(",");
            }
            // Remove the trailing comma.
            if (ds.Tables.Count > 0)
                sb.Length--;
            sb.Append("}}");
        }

        void WriteDataTable(StringBuilder sb, DataTable table)
        {
            sb.Append("{\"Rows\":[");
            foreach (DataRow row in table.Rows)
            {
                this.WriteDataRow(sb, row);
                sb.Append(",");
            }
            // Remove the trailing comma.
            if (table.Rows.Count > 0)
                --sb.Length;
            sb.Append("]}");
        }

         void WriteDataRow(StringBuilder sb, DataRow row)
        {
            sb.Append("{");
            foreach (DataColumn column in row.Table.Columns)
            {
                sb.AppendFormat("\"{0}\":", column.ColumnName);
                WriteValue(sb, row[column]);
                sb.Append(",");
            }
            if (row.Table.Rows.Count > 0)
                sb.Length--; // strip trailing ,

            sb.Append("}");
        }

        void WriteEnumerable(StringBuilder sb, IEnumerable e)
        {
            bool hasItems = false;
            sb.Append("[");

            foreach (object val in e)
            {
                WriteValue(sb, val);
                sb.Append(",");
                hasItems = true;
            }
            // Remove the trailing comma.
            if (hasItems)
            {
                --sb.Length;
            }
            sb.Append("]");
        }

        void WriteObject(StringBuilder sb, object o)
        {
            MemberInfo[] members = o.GetType().GetMembers(BindingFlags.Instance | BindingFlags.Public);
            sb.Append("{");
            bool hasMembers = false;
            foreach (MemberInfo member in members)
            {
                bool hasValue = false;
                object val = null;
                if ((member.MemberType & MemberTypes.Field) == MemberTypes.Field)
                {
                    FieldInfo field = (FieldInfo)member;
                    val = field.GetValue(o);
                    hasValue = true;
                }
                else if ((member.MemberType & MemberTypes.Property) == MemberTypes.Property)
                {
                    PropertyInfo property = (PropertyInfo)member;
                    if (property.CanRead && property.GetIndexParameters().Length == 0)
                    {
                        val = property.GetValue(o, null);
                        hasValue = true;
                    }
                }
                if (hasValue)
                {
                    sb.Append("\"");
                    sb.Append(member.Name);
                    sb.Append("\":");
                    WriteValue(sb, val);
                    sb.Append(",");
                    hasMembers = true;
                }
            }
            if (hasMembers)
            {
                --sb.Length;
            }
            sb.Append("}");
        }

        #endregion

        #region Deserialization methods

        /// <summary>
        /// High level parsing method that takes a JSON string and tries to
        /// convert it to the appropriate type. 
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="ValueType"></param>
        /// <returns></returns>
        private object ParseValueString(string Value, Type ValueType)
        {
            if (ValueType == typeof(string))
            {
                if (Value == "null")
                    return null;

                Value = Value.Replace(@"\r", "\r");
                Value = Value.Replace(@"\n", "\n");
                Value = Value.Replace(@"\\", "\\");
                Value = Value.Replace(@"\""", "\"");
                Value = Value.Replace(@"\t", "\t");
                Value = Value.Replace(@"\b", "\b");
                Value = Value.Replace(@"\f", "\f");

                // *** Strip off leading and ending quotes
                return Value.Trim('"');
            }

            // *** Most types are parsed as straight strings
            else if (ValueType == typeof(decimal) ||
                     ValueType == typeof(int) ||
                     ValueType == typeof(float) ||
                     ValueType == typeof(Single) ||
                     ValueType == typeof(double) ||
                     ValueType == typeof(long) ||
                     ValueType == typeof(bool) ||
                     ValueType == typeof(short) ||
                     ValueType == typeof(byte))
            {
                return wwUtils.StringToTypedValue(Value, ValueType);
            }
            else if (ValueType == typeof(DateTime))
            {
                if (Value == null)
                    return DateTime.MinValue;

                try
                {
                    Value = wwUtils.ExtractString(Value, "new Date(", ")");
                    string[] DateParts = Value.Split(',');

                    return new DateTime(int.Parse(DateParts[0]),
                                        int.Parse(DateParts[1]) + 1,
                                        int.Parse(DateParts[2]),
                                        int.Parse(DateParts[3]),
                                        int.Parse(DateParts[4]),
                                        int.Parse(DateParts[5]),
                                        DateTimeKind.Utc);
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
            else if (ValueType.IsArray || ValueType.GetInterface("IList") != null)
            {
                StringReader Reader = new StringReader(Value);
                return this.ParseArray(Reader, ValueType);
            }
            else if (ValueType.IsClass)
            {
                StringReader Reader = new StringReader(Value);
                return this.ParseObject(Reader, ValueType, false);
            }

            return null;
        }

        /// <summary>
        /// Parsing routine specific to parsing an object. Note the recursive flag which 
        /// allows skipping over prefix information.
        /// </summary>
        /// <param name="Reader"></param>
        /// <param name="ValueType"></param>
        /// <param name="RecursiveCall"></param>
        /// <returns></returns>
        private object ParseObject(StringReader Reader, Type ValueType, bool RecursiveCall)
        {
            // {"Company":"West Wind Technologies","Count":2,"DollarTotal":111.22,"TestValue":true,"Entered":new Date("Tuesday, January 17, 2006 9:02:26 AM UTC")}

            int Val = 0;
            char NextChar = '{';

            if (!RecursiveCall)
            {
                Reader.Read();
                if (Val == -1)
                    return null;
                NextChar = (char)Val;
            }
            else
            {
                NextChar = '{';
            }

            char LastChar = '|';

            ParseStates State = ParseStates.None;

            string CurrentProperty = "";
            StringBuilder sb = new StringBuilder();
            NameValueCollection Properties = new NameValueCollection();

            object ResultObject = Activator.CreateInstance(ValueType);

            while (true)
            {
                LastChar = NextChar;
                Val = Reader.Read();
                if (Val == -1)
                    break;
                NextChar = (char)Val;

                if (State == ParseStates.None &&
                    NextChar == '"')
                {
                    State = ParseStates.InPropertyName;
                    CurrentProperty = "";
                    continue;
                }
                if (State == ParseStates.None)
                    continue;


                if (State == ParseStates.InPropertyName && NextChar == '"')
                {
                    State = ParseStates.InPropertyValueTransition;
                }
                else if (State == ParseStates.InPropertyName)
                {
                    CurrentProperty += NextChar;
                }
                else if (State == ParseStates.InPropertyValueTransition && NextChar == ':')
                    continue;
                else if (State == ParseStates.InPropertyValueTransition && NextChar == '"')
                {
                    State = ParseStates.InStringValue;
                }
                else if (State == ParseStates.InPropertyValueTransition && NextChar == '[')
                {
                    MemberInfo[] mi = ResultObject.GetType().GetMember(CurrentProperty, BindingFlags.Instance | BindingFlags.Instance | BindingFlags.GetField | BindingFlags.GetProperty | BindingFlags.Public);
                    if (mi == null || mi.Length < 1)
                        this.AssignProperty(ResultObject, CurrentProperty, null);
                    else
                    {
                        Type ObjectType = null;
                        if (mi[0].MemberType == MemberTypes.Field)
                            ObjectType = ((FieldInfo)mi[0]).FieldType;
                        else
                            ObjectType = ((PropertyInfo)mi[0]).PropertyType;

                        object Result = this.ParseArray(Reader, ObjectType);

                        if (mi[0].MemberType == MemberTypes.Field)
                            ((FieldInfo)mi[0]).SetValue(ResultObject, Result);
                        else
                            ((PropertyInfo)mi[0]).SetValue(ResultObject, Result, null);

                        NextChar = ']';
                    }
                    State = ParseStates.None;
                }
                // *** Nested Object - recursively read characters
                else if (State == ParseStates.InPropertyValueTransition && NextChar == '{')
                {
                    State = ParseStates.InObject;
                    MemberInfo[] mi = ResultObject.GetType().GetMember(CurrentProperty, BindingFlags.Instance | BindingFlags.Instance | BindingFlags.GetField | BindingFlags.GetProperty | BindingFlags.Public);
                    if (mi == null || mi.Length < 1)
                        this.AssignProperty(ResultObject, CurrentProperty, null);
                    else
                    {
                        Type ObjectType = null;
                        if (mi[0].MemberType == MemberTypes.Field)
                            ObjectType = ((FieldInfo)mi[0]).FieldType;
                        else
                            ObjectType = ((PropertyInfo)mi[0]).PropertyType;

                        object Result = this.ParseObject(Reader, ObjectType, true);

                        if (mi[0].MemberType == MemberTypes.Field)
                            ((FieldInfo)mi[0]).SetValue(ResultObject, Result);
                        else
                            ((PropertyInfo)mi[0]).SetValue(ResultObject, Result, null);

                        NextChar = '}';
                    }
                    State = ParseStates.None;
                }
                else if (State == ParseStates.InPropertyValueTransition)
                {
                    State = ParseStates.InValue;
                    sb.Append(NextChar);
                }
                else if (State == ParseStates.InStringValue)
                {
                    if (NextChar == '"' && LastChar != '\'')
                    {
                        State = ParseStates.None;
                        this.AssignProperty(ResultObject, CurrentProperty, sb.ToString());
                        sb.Length = 0;
                    }
                    else
                        sb.Append(NextChar);
                }
                else if (State == ParseStates.InValue && NextChar == '}')
                {
                    this.AssignProperty(ResultObject, CurrentProperty, sb.ToString());
                    sb.Length = 0;
                    State = ParseStates.EndOfObject;
                    return ResultObject;
                }
                else if (State == ParseStates.InValue && NextChar == ',')
                {
                    if (Reader.Peek() != '"')
                        // *** check if hte next character is a property delimiter
                        // *** if not it's a , in the value like in dates or decimals
                        sb.Append(NextChar);
                    else
                    {
                        this.AssignProperty(ResultObject, CurrentProperty, sb.ToString());
                        sb.Length = 0;
                        State = ParseStates.None;
                    }
                }
                else if (State == ParseStates.InValue)
                    sb.Append(NextChar);

                else if (State == ParseStates.EndOfObject || NextChar == '}')
                {
                    return ResultObject;
                }
            }

            return ResultObject;
        }


        /// <summary>
        /// Parses a array subtype 
        /// </summary>
        /// <param name="Reader"></param>
        /// <param name="ArrayType"></param>
        /// <returns></returns>
        private object ParseArray(StringReader Reader, Type ArrayType)
        {
            // *** Retrieve the type of child elements - Can be Array, Collection etc.
            Type ElementType = this.GetArrayType(ArrayType);

            // *** Start by parsing each of the items
            ArrayList Items = new ArrayList();

            char NextChar = '[';
            char LastChar = '|';
            ParseStates State = ParseStates.None;

            while (true)
            {
                LastChar = NextChar;
                int Val = Reader.Read();
                if (Val == -1)
                    break;
                NextChar = (char)Val;


                // *** Adding an object
                if ((State == ParseStates.None || State == ParseStates.InPropertyValueTransition) && NextChar == '{')
                {
                    object ParsedObject = this.ParseObject(Reader, ElementType, true);
                    Items.Add(ParsedObject);
                    State = ParseStates.InPropertyValueTransition;
                }

                else if ((State == ParseStates.InPropertyValueTransition || State == ParseStates.None) && NextChar == ']')
                {
                    if (ArrayType.IsArray)
                    {

                        Array ElementArray = Activator.CreateInstance(ArrayType, Items.Count) as Array;
                        for (int i = 0; i < Items.Count; i++)
                        {
                            object Item = Activator.CreateInstance(ElementType);
                            ElementArray.SetValue(Items[i], i);
                        }
                        return ElementArray;
                    }
                    //else if (ArrayType is DataRow)
                    //{

                    //}
                    else if (ArrayType.GetInterface("IList") != null)
                    {
                        IList Col = Activator.CreateInstance(ArrayType) as IList;
                        foreach (object Item in Items)
                        {
                            Col.Add(Item);
                        }
                        return Col;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Returns the type of item type of the array/collection
        /// </summary>
        /// <param name="ArrayType"></param>
        /// <returns></returns>
        private Type GetArrayType(Type ArrayType)
        {
            if (ArrayType.IsArray)
                return ArrayType.GetElementType();

            if (ArrayType == typeof(DataTable))
                return typeof(DataRow);

            if (ArrayType.GetInterface("IList") != null)
            {
                MethodInfo Method = ArrayType.GetMethod("Add");
                ParameterInfo Parameter = Method.GetParameters()[0];
                Type ResultType = Parameter.ParameterType;
                return ResultType;
            }

            return null;

        }

        private enum ParseStates
        {
            None,
            InPropertyName,
            InPropertyValueTransition,
            InStringValue,
            InValue,
            InDate,
            InObject,
            EndOfObject
        }

        private void AssignProperty(object ResultObject, string Property, string Value)
        {
            MemberInfo[] mi = ResultObject.GetType().GetMember(Property,
                                                    BindingFlags.Instance |
                                                    BindingFlags.GetField | BindingFlags.GetProperty |
                                                    BindingFlags.IgnoreCase | BindingFlags.Public);
            if (mi == null || mi.Length < 1)
                return;

            if (mi[0].MemberType == MemberTypes.Field)
            {
                FieldInfo FInfo = mi[0] as FieldInfo;
                FInfo.SetValue(ResultObject, this.ParseValueString(Value, FInfo.FieldType));
            }
            else
            {
                PropertyInfo PInfo = mi[0] as PropertyInfo;
                PInfo.SetValue(ResultObject, this.ParseValueString(Value, PInfo.PropertyType), null);
            }
        }
        #endregion 

    }
}